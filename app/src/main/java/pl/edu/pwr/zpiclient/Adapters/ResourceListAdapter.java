package pl.edu.pwr.zpiclient.Adapters;

/**
 * Created by bartek on 24.04.2019.
 */
//zmieniłem na ogólną Activity

import android.app.Activity;
import android.content.Context;

import androidx.constraintlayout.widget.ConstraintLayout;
import pl.edu.pwr.zpiclient.DataClasses.Resource;
import pl.edu.pwr.zpiclient.R;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;



public class ResourceListAdapter extends ArrayAdapter {

    private ArrayList resources;
    private Activity mainActivity;
    private TextView resourceNameText;
    private EditText resourceAmountText;
    private ConstraintLayout cl;
    //private boolean active;

    public ResourceListAdapter(Context context, ArrayList<Resource> res) {

        super(context, 0, res);

        this.resources=res;
        this.mainActivity=(Activity) context;
    }


    @Override
    public int getCount() {
        return resources.size();
    }

    @Override
    public Object getItem(int pos) {
        return resources.get(pos);
    }

    @Override
    public long getItemId(int pos) {
        return 0;
        //just return 0 if your list items do not have an Id variable.
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final Resource resource = (Resource)getItem(position);


        /*if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.song_layout, parent, false);
        }*/
        LayoutInflater inflater = (LayoutInflater) mainActivity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.resource_list_layout, null);
        }






        resourceNameText = convertView.findViewById(R.id.resourceNameText);
        resourceAmountText = convertView.findViewById(R.id.amountLeftText);


        cl=convertView.findViewById(R.id.taskListLayoutBackground);






        resourceNameText.setText(resource.getName());
        resourceAmountText.setText("1");






        return convertView;
    }


}


