package pl.edu.pwr.zpiclient.Activities;

import android.content.Context;
import android.content.Intent;
import androidx.fragment.app.DialogFragment;
import androidx.appcompat.app.AppCompatActivity;
import pl.edu.pwr.zpiclient.Adapters.ResourceListAdapter;
import pl.edu.pwr.zpiclient.DataClasses.Resource;
import pl.edu.pwr.zpiclient.DataClasses.Task;
import pl.edu.pwr.zpiclient.DataClasses.Worker;
import pl.edu.pwr.zpiclient.FinishDialog;
import pl.edu.pwr.zpiclient.R;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import org.joda.time.Instant;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;

import static pl.edu.pwr.zpiclient.Activities.MainActivity.apiUri;

public class WorkerTaskReportActivity extends AppCompatActivity implements FinishDialog.FinishDialogListener {
    private Worker worker;
    private Task task;
    private ArrayList<Resource> resources;
    private TextView name,startDate,currDate;
    private Button reportBtn,finishBtn;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_worker_task_report);

        setContents();
        setListeners();


        setResourcesList();
    }

    public static void start(Context context, Task task, ArrayList<Resource> res, Worker worker) {
        Intent starter = new Intent(context, WorkerTaskReportActivity.class);
        starter.putExtra("resources",res);
        starter.putExtra("task", task);
        starter.putExtra("worker",worker);
        context.startActivity(starter);
    }

    private void setContents(){
        worker=(Worker)getIntent().getSerializableExtra("worker");
        task=(Task)getIntent().getSerializableExtra("task");
        resources=(ArrayList<Resource>)getIntent().getSerializableExtra("resources");

        name=findViewById(R.id.taskNameText);
        startDate=findViewById(R.id.taskStartDateText);
        currDate= findViewById(R.id.taskEndDateText);

        reportBtn=findViewById(R.id.sendReportButton);
        finishBtn=findViewById(R.id.finishTaskButton);


        String temp =Instant.now().toDateTime().toString();
        String[] temp2 = temp.split("T");
        currDate.setText(temp2[0]+" "+temp2[1].substring(0,Math.min(temp2[1].length(),5)));
        name.setText(task.getName());
        startDate.setText(task.getData());
    }

    private void finishTask(){
        DialogFragment dialog = new FinishDialog();
        dialog.show(getSupportFragmentManager(), "FinishDialog");
    }

    @Override
    public void onDialogNegativeClick(DialogFragment dialog) {

    }

    @Override
    public void onDialogPositiveClick(DialogFragment dialog) {
        finishTask2();
    }

    private void finishTask2(){
        RestTemplate restTemplate = RTemplate.getInstance().restTemplate;
        HttpHeaders headers=RTemplate.getInstance().sessionHeaders;

        headers.add("idS", "3");
        headers.add("idT", String.valueOf(task.getIdTask()));
        HttpEntity request=new HttpEntity<String>(headers);

        restTemplate.exchange(apiUri + "updateTaskS2", HttpMethod.GET, request, String.class).getBody();
        //Toast.makeText(getApplicationContext(), response,Toast.LENGTH_SHORT).show();

        headers.remove("idS");
        headers.remove("idT");


        finish();


    }

    private void setListeners(){
        finishBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finishTask();
            }
        });
    }

    private void setResourcesList(){

        ResourceListAdapter resAdap = new ResourceListAdapter(this, resources);

        ListView resourcesL = findViewById(R.id.resourceListView);

        resourcesL.setAdapter(resAdap);
    }


}
