package pl.edu.pwr.zpiclient.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;
import pl.edu.pwr.zpiclient.AddDataDialog;
import pl.edu.pwr.zpiclient.DataClasses.MyJ48;
import pl.edu.pwr.zpiclient.DataClasses.TrimmedData;
import pl.edu.pwr.zpiclient.DataClasses.Worker;
import pl.edu.pwr.zpiclient.MyFileWriter;
import pl.edu.pwr.zpiclient.R;
import pl.edu.pwr.zpiclient.RemoveDataDialog;
import pl.edu.pwr.zpiclient.Services.BackgroundService;
import weka.classifiers.trees.J48;
import weka.core.Instances;
import weka.core.converters.CSVLoader;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

public class WorkerModelActivity extends AppCompatActivity implements AddDataDialog.AddDataDialogListener, RemoveDataDialog.RemoveDataDialogListener {

    private Worker worker;
    private Button inWorkBtn,offWorkBtn;
    public BackgroundService backService;
    private TextView locationText,wifiText,timeText,blueText,predText;
    private Switch trackingSwitch;
    private SharedPreferences pref;

    private Intent backgroundIntent;
    private PendingIntent pintent;
    private Timer timer;

    private float lon,lat,alt;
    //private String wifiT,blueT;
    private String pred;
    private boolean offline,inWork, clear=false;
    private int classif;


    private static MyJ48 myJ48;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_worker_model);

        Toolbar toolbar =  findViewById(R.id.alarmToolbar);

        setSupportActionBar(toolbar);



        setContents();
        setListeners();

        worker=((Worker)getIntent().getSerializableExtra("worker"));


        pref = PreferenceManager.getDefaultSharedPreferences(WorkerModelActivity.this);
        myJ48 = new MyJ48(worker.getLogin());

        backgroundIntent = new Intent(this, BackgroundService.class);
        backgroundIntent.putExtra("worker",worker.getLogin());



        pintent = PendingIntent.getService(this, 0, backgroundIntent, 0);


        //final Intent intent = new Intent(this.getApplication(), BackgroundService.class);
        //this.getApplication().startService(intent);
//        this.getApplication().startForegroundService(intent);
        //this.getApplication().bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE);



        //alarm.setExact(AlarmManager.RTC_WAKEUP,1000,pintent);
        //alarm.setExact(AlarmManager.RTC_WAKEUP,5000,pintent);

        //readModel(WorkerModelActivity.this);



        timer = new Timer();
        TimerTask timerTaskObj = new TimerTask() {

            public void run() {






                lat = (pref.getFloat("Latitude",0));
                lon = (pref.getFloat("Longitude",0));
                alt = pref.getFloat("Altitude",0);
                Set<String> wifi = pref.getStringSet("Wifi", new HashSet<String>());
                final StringBuilder wifiSB = new StringBuilder();
                wifiSB.append("Wifi: \n");
                //wifiT = "Wifi: \n";
                for (String single:wifi
                ) {
                    wifiSB.append(single+"\n");

                }
                Set<String> blue = pref.getStringSet("Blue", new HashSet<String>());
                final StringBuilder blueSB = new StringBuilder();
                blueSB.append("Bluetooth: \n");
                //blueT = "Bluetooth: \n";
                for (String single:blue
                ) {
                    blueSB.append(single+"\n");
                    //blueT+=single+"\n";

                }





                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        //locationText.setText(Float.toString(pref.getFloat("Latitude",0)));
                        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        timeText.setText(dateFormat.format(new Date(pref.getLong("Time",0))));

                        locationText.setText("Szerokość: "+lat+", Długość"+lon);

                        wifiText.setText(wifiSB);


                        blueText.setText(blueSB);
                        pred=pref.getString("pred","Brak danych");

                        predText.setText(pred);

                        if(pred.equals("Praca")){
                            predText.setTextColor(ContextCompat.getColor(WorkerModelActivity.this,R.color.lightgreen));

                        }
                        else if(pred.equals("Nie Praca")){
                            predText.setTextColor(ContextCompat.getColor(WorkerModelActivity.this,R.color.lightyellow));

                        }
                        else {
                            predText.setTextColor(ContextCompat.getColor(WorkerModelActivity.this,R.color.light));

                        }





                    }
                });
            }
        };
        TimerTask backServiceTimer = new TimerTask() {
            @Override
            public void run() {
                if(trackingSwitch.isChecked()){
                    startService(backgroundIntent);
                }
            }
        };
        timer.schedule(timerTaskObj, 0, 1000);
        timer.schedule(backServiceTimer,0,5000);




        //RestTemplate restTemplate = RTemplate.getInstance().restTemplate;


    }

    @Override
    protected void onPause(){
        super.onPause();
        if(timer != null){
            timer.cancel();
        }
    }

    public static void start(Context context, Worker worker) {
        Intent starter = new Intent(context, WorkerModelActivity.class);
        starter.putExtra("worker",worker);
        context.startActivity(starter);
    }

    private void setContents(){
        inWorkBtn = findViewById(R.id.inWorkBtn);
        offWorkBtn = findViewById(R.id.offWorkBtn);
        locationText=findViewById(R.id.locationText);
        wifiText=findViewById(R.id.wifiText);
        timeText=findViewById(R.id.timeText);
        blueText=findViewById(R.id.blueText);
        predText=findViewById(R.id.predictText);
        trackingSwitch=findViewById(R.id.trackingSwitch);

        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(WorkerModelActivity.this);

        trackingSwitch.setChecked(pref.getBoolean("tracking",true));

    }

    private void setListeners(){
        inWorkBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                inWork=true;
                DialogFragment dialog = new AddDataDialog();
                dialog.show(getSupportFragmentManager(), "AddDataDialog");

                }

        });

        offWorkBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                inWork=false;
                DialogFragment dialog = new AddDataDialog();
                dialog.show(getSupportFragmentManager(), "AddDataDialog");
            }
        });

        trackingSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean tracking) {
                SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(WorkerModelActivity.this);
                SharedPreferences.Editor edit = pref.edit();
                edit.putBoolean("tracking",tracking);
                edit.apply();

                if(tracking){
                    startTracking();
                }
                else {
                    stopTracking();
                }
            }
        });
    }

    private void startTracking(){

        AlarmManager alarm = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
        //alarm.cancel(pintent);
        alarm.setInexactRepeating(AlarmManager.RTC_WAKEUP, 0,60000, pintent);
    }

    private void stopTracking(){
        AlarmManager alarm = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
        alarm.cancel(pintent);
    }

    @Override
    public boolean onCreateOptionsMenu (Menu menu){
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.model_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected (MenuItem item){
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_alarm_create) {

            myJ48.createModel(WorkerModelActivity.this);
            myJ48.saveModel(WorkerModelActivity.this);











        }

        else if (id==R.id.action_alarm_reset){


            clear=true;
            DialogFragment dialog = new RemoveDataDialog();
            dialog.show(getSupportFragmentManager(), "RemoveDataDialog");


        }

        return super.onOptionsItemSelected(item);
    }





    @Override
    public void onDialogPositiveClick(DialogFragment dialog) {
        if(clear) {
            new MyFileWriter().clearFile(worker.getLogin() + "data", true, WorkerModelActivity.this);
            clear=false;
            myJ48.createModel(WorkerModelActivity.this);
            myJ48.saveModel(WorkerModelActivity.this);
        }else {
            if(inWork){
                saveInWork();
            }
            else {
                saveOffWork();
            }
        }

    }

    @Override
    public void onDialogNegativeClick(DialogFragment dialog) {

        clear=false;
    }

    private void saveInWork(){
        float lat = pref.getFloat("Latitude", 0);
        float lon = pref.getFloat("Longitude", 0);
        float alt = pref.getFloat("Altitude", 0);
        Set<String> blue = pref.getStringSet("Blue", new HashSet<String>());
        Set<String> wifi = pref.getStringSet("Wifi", new HashSet<String>());
        long time=pref.getLong("Time",0);

        String[] wifiArray = wifi.toArray(new String[0]);
        wifiArray=Arrays.copyOf(wifiArray,3);

        String[] blueArray = blue.toArray(new String[0]);
        blueArray= Arrays.copyOf(blueArray,3);

        TrimmedData curr=new TrimmedData(new Date(time),lat,lon,alt,wifiArray,blueArray,"Praca");
        new MyFileWriter().writeToFile(worker.getLogin()+"data",curr.toString(), WorkerModelActivity.this);
    }

    private void saveOffWork(){
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(WorkerModelActivity.this);
        float lat = pref.getFloat("Latitude", 0);
        float lon = pref.getFloat("Longitude", 0);
        float alt = pref.getFloat("Altitude", 0);
        Set<String> blue = pref.getStringSet("Blue", new HashSet<String>());
        Set<String> wifi = pref.getStringSet("Wifi", new HashSet<String>());
        long time=pref.getLong("Time",0);
        String[] wifiArray = wifi.toArray(new String[0]);
        wifiArray=Arrays.copyOf(wifiArray,3);

        String[] blueArray = blue.toArray(new String[0]);
        blueArray= Arrays.copyOf(blueArray,3);

        TrimmedData fal=new TrimmedData(new Date(time),lat,lon,alt,wifiArray,blueArray,"Nie praca");
        MyFileWriter mfw = new MyFileWriter();
        mfw.writeToFile(worker.getLogin()+"data",fal.toString(), WorkerModelActivity.this);

        /*lat+=10;
        lon+=10;
        fal=new TrimmedData(new Date(time),lat,lon,alt,wifiArray,blueArray,"Nie praca");

        mfw.writeToFile(worker.getLogin()+"data",fal.toString(), WorkerModelActivity.this);

        lat-=20;
        fal=new TrimmedData(new Date(time),lat,lon,alt,wifiArray,blueArray,"Nie praca");



        mfw.writeToFile(worker.getLogin()+"data",fal.toString(), WorkerModelActivity.this);
        lon-=20;
        fal=new TrimmedData(new Date(time),lat,lon,alt,wifiArray,blueArray,"Nie praca");
        mfw.writeToFile(worker.getLogin()+"data",fal.toString(), WorkerModelActivity.this);
        lat+=20;
        fal=new TrimmedData(new Date(time),lat,lon,alt,wifiArray,blueArray,"Nie praca");
        mfw.writeToFile(worker.getLogin()+"data",fal.toString(), WorkerModelActivity.this);*/
    }


}
