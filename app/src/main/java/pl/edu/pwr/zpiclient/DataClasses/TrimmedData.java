package pl.edu.pwr.zpiclient.DataClasses;

import android.util.Log;

import com.google.android.gms.common.util.ArrayUtils;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Set;

import androidx.annotation.NonNull;

public class TrimmedData implements Serializable {
    private float lat,lon,alt;
    private String[] blue;
    private String[] wifi;
    private String category;
    private Date date;
    private String status;
    private DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public TrimmedData( Date date, float lat,float lon, float alt, String[] wifi, String[] blue, String cat){
        this.lat=lat;
        this.lon=lon;
        this.alt=alt;
        this.blue= Arrays.copyOf(blue,3);
        this.wifi=Arrays.copyOf(wifi,3);
        category=cat;
        this.date=date;
        status="None";
    }

    public TrimmedData(){
        lat=0;
        lon=0;
        alt=0;
        date=new Date(Calendar.getInstance().getTimeInMillis());
        wifi=new String[3];
        blue=new String[3];
        category="Nie Praca";
        status="Not Sent";
    }

    public TrimmedData( String date, float lat,float lon, float alt, String[] wifi, String[] blue, String cat){
        this.lat=lat;
        this.lon=lon;
        this.alt=alt;
        //this.blue= Arrays.copyOf(blue,3);
        this.blue=new String[3];
        for (int i=0;i<3;i++){
            if(blue[i]==null){
                this.blue[i]="null";
            }else {
                this.blue[i]=blue[i];
            }
        }
        this.wifi=Arrays.copyOf(wifi,3);
        category=cat;

        try {
            this.date = dateFormat.parse(date);
        }catch (Exception e){
            Log.e("DateFormatError",e.toString());
        }

        status="None";
    }

    public TrimmedData(String csv){
        try{
            String[] split = csv.split(",");

            this.date = dateFormat.parse(split[0]);
            this.lat = Float.parseFloat(split[1]);
            this.lon = Float.parseFloat(split[2]);
            this.alt = Float.parseFloat(split[3]);
            this.wifi = Arrays.copyOfRange(split,4,7);
            this.blue = Arrays.copyOfRange(split,7,10);
            this.category = split[10];
            if(split.length>11){
                status=split[11];
            }else {
                status="Not Sent";
            }
        }catch (Exception e){
            Log.e("Error parsing CSV String",e.toString());

        }
    }

    @NonNull
    @Override
    public String toString() {

        String strDate = dateFormat.format(date);
        return strDate+","+lat+","+lon+","+alt+","+wifi[0]+","+wifi[1]+","+wifi[2]+","+blue[0]+","+blue[1]+","+blue[2]+","+category;
    }

    public String toStatusSting(){

        String strDate = dateFormat.format(date);
        return strDate+","+lat+","+lon+","+alt+","+wifi[0]+","+wifi[1]+","+wifi[2]+","+blue[0]+","+blue[1]+","+blue[2]+","+category+","+status;

    }

    public String toTrimmedString(){
        return lat+","+lon+","+alt+","+wifi[0]+","+wifi[1]+","+wifi[2]+","+blue[0]+","+blue[1]+","+blue[2];

    }

    public String getStringDate(){


        return dateFormat.format(date);
    }

    public void setCategory(String cat){
        category=cat;
    }

    public String getCategory(){
        return category!=null?category:"Brak";
    }

    public long getDateLong(){
        return date.getTime();
    }

    public float getLat(){return lat;}

    public float getLon(){return lon;}

    public float getAlt(){return alt;}

    public String[] getStrings(){
        String[] sum = new String[7];
        sum[0]=wifi[0];
        sum[1]=wifi[1];
        sum[2]=wifi[2];
        sum[3]=blue[0];
        sum[4]=blue[1];
        sum[5]=blue[2];
        sum[6]=category;

        for(int i=0;i<7;i++){
            if(sum[i]==null){
                sum[i]="null";
            }
        }
        return sum;
    }

    public String getStatus(){
        return status;
    }

    public void setStatus(String stat){
        status=stat;
    }

    public String[] getWifi(){
        return wifi;
    }

    public String[] getBlue(){
        return blue;
    }
}
