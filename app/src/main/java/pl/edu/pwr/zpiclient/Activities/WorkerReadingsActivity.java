package pl.edu.pwr.zpiclient.Activities;

import androidx.appcompat.app.AppCompatActivity;
import pl.edu.pwr.zpiclient.Adapters.ReadingsListAdapter;
import pl.edu.pwr.zpiclient.DataClasses.TrimmedData;
import pl.edu.pwr.zpiclient.DataClasses.Worker;
import pl.edu.pwr.zpiclient.R;
import pl.edu.pwr.zpiclient.Services.BackgroundService;
import pl.edu.pwr.zpiclient.Services.ReadingsTransferService;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashMap;

import static pl.edu.pwr.zpiclient.Activities.MainActivity.apiUri;

public class WorkerReadingsActivity extends AppCompatActivity {
    private Button sendDataButton, modelSettingsButton;
    private ArrayList<TrimmedData> readings = new ArrayList<>();
    private ArrayList<TrimmedData> lastRead = new ArrayList<>();
    private ListView readingsList,lastSentReadingsList;
    private ReadingsListAdapter readingsAdapter,lastSentAdapter;
    private Worker worker;
    private boolean online;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_worker_readings);

        worker=((Worker)getIntent().getSerializableExtra("worker"));
        online=getIntent().getBooleanExtra("online",false);
        Log.i("WorkerLogin",worker.getLogin());

        data();
        setContents();
        setListeners();
    }

    @Override
    protected void onResume(){
        super.onResume();
        data();
        lastSentAdapter.notifyDataSetChanged();
        readingsAdapter.notifyDataSetChanged();
    }

    public static void start(Context context, Worker worker,boolean online) {
        Intent starter = new Intent(context, WorkerReadingsActivity.class);
        starter.putExtra("worker",worker);
        starter.putExtra("online",online);
        context.startActivity(starter);
    }

    private void data(){
        readData();
        if(online) {
            getData();
        }

    }
    private void readData(){

        try (BufferedReader br = new BufferedReader(new InputStreamReader(openFileInput(worker.getLogin()+"change.txt")))) {
            readings.clear();
            String line;
            //line = br.readLine();
            while ((line = br.readLine()) != null) {
                Log.i("Line Read:",line);

                TrimmedData reading = new TrimmedData(line);
                readings.add(reading);

                Log.i("Status: ",reading.getStatus());

            }
            Collections.reverse(readings);
        }catch (Exception e){
            Log.e("Error reading data",e.toString());
        }
    }

    private void setContents(){
        readingsList = findViewById(R.id.readingsListView);
        lastSentReadingsList=findViewById(R.id.lastReadingSentListView);

        readingsAdapter = new ReadingsListAdapter(this,readings);
        lastSentAdapter = new ReadingsListAdapter(this,lastRead);

        readingsList.setAdapter(readingsAdapter);
        lastSentReadingsList.setAdapter(lastSentAdapter);

        sendDataButton=findViewById(R.id.sendReadingsBtn);
        modelSettingsButton=findViewById(R.id.modelSettingsBtn);
    }

    private void setListeners(){
        sendDataButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent sendingIntent = new Intent(WorkerReadingsActivity.this, ReadingsTransferService.class);
                sendingIntent.putExtra("worker",worker.getLogin());
                sendingIntent.putExtra("workerID",Long.toString(worker.getId()));
                sendingIntent.putExtra("online",online);

                startService(sendingIntent);

                WorkerReadingsActivity.this.onResume();
            }
        });
        modelSettingsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                WorkerModelActivity.start(WorkerReadingsActivity.this,worker);
            }
        });
    }

    private void getData(){
        Log.i("Getting data",worker.getId()+"");
        RestTemplate restTemplate = RTemplate.getInstance().restTemplate;
        lastRead = new ArrayList<>();
        try{
            Log.i("returnWid"," "+worker.getId());

        ArrayList<LinkedHashMap> response = restTemplate.exchange(apiUri+"workerTimestamp/"+worker.getId(), HttpMethod.GET, new HttpEntity<String>(RTemplate.getInstance().sessionHeaders), ArrayList.class).getBody();

        //for (int i = 0; i < response.size(); i++) {
            LinkedHashMap hm = (LinkedHashMap) response.get(0);

            ArrayList<String> strings = (ArrayList)hm.get("strings");
            //Log.i("Returned Strings:",strings.get(0));
            TrimmedData data = new TrimmedData(new Date((long)hm.get("time")),((Double)hm.get("float1")).floatValue(),((Double)hm.get("float2")).floatValue(),((Double)hm.get("float3")).floatValue(), strings.subList(0,3).toArray(new String[0]),strings.subList(3,6).toArray(new String[0]),strings.get(6));
            //Log.i("Last Sent Response: ",data.toString());
            lastRead.add(data);
            //Group g = new Group((int) (hm.get("idGroup")), (String) (hm.get("name")), (String) (hm.get("description")),
            //        (int) (hm.get("workerId")));

            //if(worker.getId()==g.getWorkerId()) {
            //groups.add(g);
            //}
        }
        catch (Exception e){
            Log.e("ServerException: ",e.toString());
        }
    }


}
