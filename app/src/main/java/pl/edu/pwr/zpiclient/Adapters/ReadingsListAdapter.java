package pl.edu.pwr.zpiclient.Adapters;

/**
 * Created by bartek on 24.04.2019.
 */


import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import pl.edu.pwr.zpiclient.DataClasses.TrimmedData;
import pl.edu.pwr.zpiclient.R;
import pl.edu.pwr.zpiclient.Activities.WorkerReadingsActivity;


public class ReadingsListAdapter extends ArrayAdapter {

    private ArrayList<TrimmedData> readings;
    private WorkerReadingsActivity mainActivity;
    private TextView dateText,classText, lonText,latText, blueText,blue2Text,blue3Text, wifiText,wifi2Text,wifi3Text;

    private ConstraintLayout cl;


    public ReadingsListAdapter(Context context, ArrayList<TrimmedData> readings) {

        super(context, 0, readings);

        this.readings=readings;
        this.mainActivity=(WorkerReadingsActivity) context;

    }




    @Override
    public int getCount() {
        return readings.size();
    }

    @Override
    public TrimmedData getItem(int pos) {
        return readings.get(pos);
    }

    @Override
    public long getItemId(int pos) {
        return 0;
        //just return 0 if your list items do not have an Id variable.
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final TrimmedData data =getItem(position);


        /*if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.song_layout, parent, false);
        }*/
        LayoutInflater inflater = (LayoutInflater) mainActivity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.reading_list_layout, null);
        }



        dateText=convertView.findViewById(R.id.dateText);
        classText=convertView.findViewById(R.id.classText);
        latText=convertView.findViewById(R.id.latText);
        lonText=convertView.findViewById(R.id.lonText);
        wifiText=convertView.findViewById(R.id.wifiText);
        wifi2Text=convertView.findViewById(R.id.wifi2Text);
        wifi3Text=convertView.findViewById(R.id.wifi3Text);
        blueText=convertView.findViewById(R.id.bluetoothText);
        blue2Text=convertView.findViewById(R.id.bluetooth2Text);
        blue3Text=convertView.findViewById(R.id.bluetooth3Text);





        cl=convertView.findViewById(R.id.readingsListLayoutBackground);

        String status = data.getStatus();

        if(status.equals("Sent")){
            cl.setBackgroundColor(ContextCompat.getColor(mainActivity,R.color.lightgreen));
        }
        else if(status.equals("Not Sent")){
            cl.setBackgroundColor(ContextCompat.getColor(mainActivity,R.color.lightyellow));
        }


        String cat=data.getCategory();
        //Log.i("AdapterData: ",cat);
        String lonStr = "Długość: "+String.format("%.5f",data.getLon());
        String latStr = "Szerokość: "+String.format("%.5f",data.getLat());



        dateText.setText(data.getStringDate());
        classText.setText(cat);
        lonText.setText(lonStr);
        latText.setText(latStr);
        String[] wifi = data.getWifi();
        String[] blue = data.getBlue();
        if(wifi[0].equals("null"))
            wifiText.setVisibility(View.INVISIBLE);
        wifiText.setText(wifi[0]);


        if(wifi[1].equals("null"))
            wifi2Text.setVisibility(View.INVISIBLE);
        wifi2Text.setText(wifi[1]);


        if(wifi[2].equals("null"))
            wifi3Text.setVisibility(View.INVISIBLE);
        wifi3Text.setText(wifi[2]);


        if(blue[0].equals("null"))
            blueText.setVisibility(View.INVISIBLE);
        blueText.setText(blue[0]);
        if(blue[1].equals("null"))
            blue2Text.setVisibility(View.INVISIBLE);
        blue2Text.setText(blue[1]);
        if(blue[2].equals("null"))
            blue3Text.setVisibility(View.INVISIBLE);
        blue3Text.setText(blue[2]);













        return convertView;
    }




}


