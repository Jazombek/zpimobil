package pl.edu.pwr.zpiclient.DataClasses;

import java.io.Serializable;

/**
 * Created by bartek on 11.06.2019.
 */

public class TaskWorker extends Task implements Serializable {


    protected int idWorkerStatus;

    public TaskWorker() {
    }

    public TaskWorker(int idTask, String name, String description, String data, int idSupTask, int idManager, double progress, int idStatus, int wokerStatus) {
        super(idTask,  name, description,  data,  idSupTask, idManager,  progress,  idStatus);
        this.idWorkerStatus=wokerStatus;
    }



    public int getIdWorkerStatus() {
        return idWorkerStatus;
    }

    public void setIdWorkerStatus(int idWorkerStatus) {
        this.idWorkerStatus=idWorkerStatus;
    }





}