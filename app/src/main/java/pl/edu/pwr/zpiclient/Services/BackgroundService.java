package pl.edu.pwr.zpiclient.Services;

import android.Manifest;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;

import android.preference.PreferenceManager;
import android.util.Log;


import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import pl.edu.pwr.zpiclient.DataClasses.MyJ48;
import pl.edu.pwr.zpiclient.DataClasses.SensorsData;
import pl.edu.pwr.zpiclient.DataClasses.TrimmedData;
import pl.edu.pwr.zpiclient.MyFileWriter;


public class BackgroundService extends Service {
    private final LocationServiceBinder binder = new LocationServiceBinder();
    private final String TAG = "BackgroundService";
    private LocationListener mLocationListener;
    private LocationManager mLocationManager;
    private BluetoothAdapter mBluetoothAdapter;

    private static Location lastLoc;
    private ArrayList<String> mDeviceList = new ArrayList<>();
    private ArrayList<String> arraylist;
    WifiManager wifi;
    List<ScanResult> results;
    private SensorsData sensorsData;
    private final int LOCATION_INTERVAL = 1000;
    private final int LOCATION_DISTANCE = 10;
    SharedPreferences pref;
    SharedPreferences.Editor edit;

    private float lat,lon,alt;
    private String[] wifiArray,blueArray=new String[3];
    private String workerLogin;

    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    private class LocationListener implements android.location.LocationListener {

        private final String TAG = "LocationListener";
        private Location mLastLocation;

        public LocationListener(String provider) {
            mLastLocation = new Location(provider);
        }

        @Override
        public void onLocationChanged(Location location) {
            edit=pref.edit();


            mLastLocation = location;
            //lastLoc = location;
            Log.i(TAG, "LocationChanged: " + location);

            lat=(float)location.getLatitude();
            lon=(float) location.getLongitude();
            alt=(float) location.getAltitude();

            edit.putFloat("Latitude", lat);
            edit.putFloat("Longitude", lon);
            edit.putFloat("Altitude", alt);
            edit.putLong("Time", Calendar.getInstance().getTimeInMillis());

            //mDeviceList.clear();

            /*mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
            mBluetoothAdapter.startDiscovery();

            IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
            registerReceiver(mReceiver, filter);
            Set<String> blueHash = new HashSet<>(mDeviceList);

            edit.putStringSet("Blue", blueHash);

            wifi.startScan();
            arraylist = new ArrayList<>();
            try {
                int size = results.size() - 1;


                while (size >= 0) {


                    arraylist.add(results.get(size).SSID);
                    size--;
                }
            } catch (Exception e) {

            }
            Set<String> wifiHash = new HashSet<>(arraylist);
            edit.putStringSet("Wifi", wifiHash);*/


            edit.apply();
            //stopTracking();
        }

        @Override
        public void onProviderDisabled(String provider) {
            Log.e(TAG, "onProviderDisabled: " + provider);
        }

        @Override
        public void onProviderEnabled(String provider) {
            Log.e(TAG, "onProviderEnabled: " + provider);
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
            Log.e(TAG, "onStatusChanged: " + status);
        }
    }

    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                BluetoothDevice device = intent
                        .getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                mDeviceList.add(device.getName());
                Log.i("BT", device.getName());

            }
            else if(BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)){
                Set<String> blueHash = new HashSet<>(mDeviceList);

                blueArray = blueHash.toArray(new String[0]);

                edit.putStringSet("Blue", blueHash);
                edit.apply();
            }
        }
    };

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        workerLogin=intent.getStringExtra("worker");
        Log.i("BackServiceStarted: ",new TrimmedData().getStringDate());
        startTracking();

        predict();
        stopTracking();

        return START_NOT_STICKY;

    }

    @Override
    public void onCreate() {
        Log.i(TAG, "onCreateBackgroundService");
        pref=PreferenceManager.getDefaultSharedPreferences(BackgroundService.this);

        wifi = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        if (!wifi.isWifiEnabled()) {

            wifi.setWifiEnabled(true);
        }
        registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context c, Intent intent) {
                results = wifi.getScanResults();

            }
        }, new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
        startForeground(12345678, getNotification());

        //startTracking();


    }

    @Override
    public void onDestroy()
    {
        //Toast.makeText(BackgroundService.this,"Destroyed",Toast.LENGTH_SHORT).show();

        if (mLocationManager != null) {
            try {
                mLocationManager.removeUpdates(mLocationListener);
            } catch (Exception ex) {
                Log.i(TAG, "fail to remove location listners, ignore", ex);
            }
        }

        if(mBluetoothAdapter != null){
            try{
                mBluetoothAdapter.cancelDiscovery();
                Set<String> blueHash = new HashSet<>(mDeviceList);

                blueArray = blueHash.toArray(new String[0]);

                edit.putStringSet("Blue", blueHash);
                edit.apply();

            }
            catch (Exception e){
                Log.i(TAG, "failed to stop bluetooth", e);
            }
        }
        //unregisterReceiver(mReceiver);
        Log.i("Background service destroyed.","Hopefully");
        super.onDestroy();
    }

    private void initializeLocationManager() {
        if (mLocationManager == null) {
            mLocationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        }
    }

    public void startTracking() {
        edit=pref.edit();

        //Toast.makeText(this,"Service restarted",Toast.LENGTH_LONG).show();
        initializeLocationManager();

        mLocationListener = new LocationListener(LocationManager.GPS_PROVIDER);

        try {
            mLocationManager.requestLocationUpdates( LocationManager.GPS_PROVIDER, LOCATION_INTERVAL, LOCATION_DISTANCE, mLocationListener );

        } catch (SecurityException ex) {
            // Log.i(TAG, "fail to request location update, ignore", ex);
        } catch (IllegalArgumentException ex) {
            // Log.d(TAG, "gps provider does not exist " + ex.getMessage());
        }


        if (mLocationManager != null) {
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    Activity#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for Activity#requestPermissions for more details.
                return;
            }
            Location lastKnownLocationGPS = mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if (lastKnownLocationGPS != null) {
                lastLoc=lastKnownLocationGPS;
                //String delay = Long.toString(SystemClock.elapsedRealtimeNanos()-lastLoc.getElapsedRealtimeNanos());
                //Toast.makeText(BackgroundService.this, delay ,Toast.LENGTH_SHORT).show();

                lat=(float)lastLoc.getLatitude();
                lon=(float)lastLoc.getLongitude();
                alt=(float)lastLoc.getAltitude();
                edit.putFloat("Latitude", lat);
                edit.putFloat("Longitude", lon);
                edit.putFloat("Altitude", alt);
                edit.putLong("Time", Calendar.getInstance().getTimeInMillis());


            }
        }

        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        mBluetoothAdapter.startDiscovery();

        IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
        registerReceiver(mReceiver, filter);

        /*Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();

        List<String> pairedStrings = new ArrayList<>();
        for(BluetoothDevice bt : pairedDevices)
            pairedStrings.add(bt.getName());*/




        wifi.startScan();

        Comparator<ScanResult> comparator = new Comparator<ScanResult>() {
            @Override
            public int compare(ScanResult lhs, ScanResult rhs) {
                return (Integer.compare(lhs.level, rhs.level));
            }
        };


        arraylist = new ArrayList<>();
        try {
            Collections.sort(results, comparator);
            int size = results.size() - 1;
            if (size>3)
                size=3;
            int i=0;


            while (i<size) {


                arraylist.add(results.get(i).SSID);
                i++;
            }
        } catch (Exception e) {

            Log.e("Error sorting Wifi",e.toString());
        }
        Set<String> wifiHash = new HashSet<>(arraylist);
        wifiArray = wifiHash.toArray(new String[0]);
        edit.putStringSet("Wifi", wifiHash);

        //TrimmedData curr=new TrimmedData(new Date(Calendar.getInstance().getTimeInMillis()),lat,lon,alt,wifiArray,blueArray,"Brak");

        edit.apply();

        //Toast.makeText(BackgroundService.this, Integer.toString(arraylist.size()),Toast.LENGTH_SHORT).show();

        //stopTracking();



    }

    private void predict(){

        MyJ48 myJ48 = new MyJ48(workerLogin);


        TrimmedData curr=new TrimmedData(new Date(Calendar.getInstance().getTimeInMillis()),lat,lon,alt,wifiArray,blueArray,"Brak");

        myJ48.readModel(this);
        TrimmedData pred = myJ48.predict(this,curr);

        String classifCl = pref.getString("lastClassStr","");
        String predictString = pred.getCategory();
        if(!classifCl.equals(predictString)){
            //edit.putInt("lastClass",(int)j48.classifyInstance(current.get(0)));
            edit.putString("lastClassStr",predictString);


            new MyFileWriter().writeToFile(workerLogin+"change",pred.toString()+",Not Sent", this);
            edit.apply();
        }
    }

    /*private void predict2(){
        edit=pref.edit();
        CSVLoader loader = new CSVLoader();
        try {
            ObjectInputStream oin = new ObjectInputStream(openFileInput(workerLogin+"j48.txt"));
            J48 j48=(J48)oin.readObject();

            //Log.i("model",j48.toString());

            File temp=File.createTempFile(workerLogin+"curr","txt", getApplicationContext().getCacheDir());
            //FileOutputStream fos = new FileOutputStream(temp);
            FileWriter fw = new FileWriter(temp,true);
            fw.write("Time,Lat,Lon,Alt,Wifi1,Wifi2,Wifi3,Blue1,Blue2,Blue3,Class\n");
            //FileCopyUtils.copy(openFileInput(workerLogin+"data.txt"),fos);
            //wifiArray[0]="Nowa";
            //blueArray=new String[3];
            //blueArray[0]="Przykład";

            TrimmedData curr=new TrimmedData(new Date(Calendar.getInstance().getTimeInMillis()),lat,lon,alt,wifiArray,blueArray,"Brak");


            //Instances current=loader.getDataSet();


            loader.setSource(openFileInput(workerLogin+"data.txt"));

            Instances train=loader.getDataSet();

            String[] strings =curr.getStrings();

            try {

                if (train.attribute("Wifi1").indexOfValue(strings[0]) == -1) {
                    strings[0] = "null";
                }
                if (train.attribute("Wifi2").indexOfValue(strings[1]) == -1) {
                    strings[1] = "null";
                }
                if (train.attribute("Wifi3").indexOfValue(strings[2]) == -1) {
                    strings[2] = "null";
                }

                if (train.attribute("Blue1").indexOfValue(strings[3]) == -1) {
                    strings[3] = "null";
                }
                if (train.attribute("Blue2").indexOfValue(strings[4]) == -1) {
                    strings[4] = "null";
                }
                if (train.attribute("Blue3").indexOfValue(strings[5]) == -1) {
                    strings[5] = "null";
                }
            }catch (Exception e){
                Log.e("Ytho",e.toString());
            }
            //TrimmedData tempData=new TrimmedData(new Date(Calendar.getInstance().getTimeInMillis()),lat,lon,alt,wifiArray,blueArray,"Brak");

            Log.i("TempData: ",curr.toString());

            fw.write(curr.toString());
            //fw.write(lat+","+lon+","+alt+","+wifiArray[0]+","+wifiArray[1]+","+wifiArray[2]+","+blueArray[0]+","+blueArray[1]+","+blueArray[2]+","+"none");
            fw.flush();
            fw.close();


            loader.setSource(temp);
            Instances current=loader.getDataSet();
            //current.deleteAttributeAt(0);
            current.setClassIndex(current.numAttributes()-1);
            Log.i("Curr: ",current.toString());
            //Instance currInstance = current.instance(0);




            train.add(current.instance(0));

            try {
                train.instance(train.size() - 1).setValue(4, strings[0]);
                train.instance(train.size() - 1).setValue(5, strings[1]);
                train.instance(train.size() - 1).setValue(6, strings[2]);

                train.instance(train.size() - 1).setValue(7, strings[3]);
                train.instance(train.size() - 1).setValue(8, strings[4]);
                train.instance(train.size() - 1).setValue(9, strings[5]);
            }catch (Exception e){
                Log.e("Y here: ",e.toString());
            }

            //Log.i("Instance:",current.instance(0).toString(4));


            train.deleteAttributeAt(0);
            train.setClassIndex(train.numAttributes()-1);

            //Instances labeled = new Instances(train);

            //train.instance(train.size()-1).setValue(3,"null");


            //int classif = pref.getInt("lastClass",-1);
            String classifCl = pref.getString("lastClassStr","");

            Log.i("Current data:",train.instance(train.size()-1).toString());
            try {

                double classVal=j48.classifyInstance(train.instance(train.size()-1));
                Log.i("double val:",Double.toString(classVal));

                //labeled.instance(train.size()-1).setClassValue(classVal);
                String pred = train.classAttribute().value((int)classVal);
                Log.i("Classif,Pred: ",classifCl+","+pred);
                edit.putString("pred",pred);
                if(!classifCl.equals(pred)){
                    //edit.putInt("lastClass",(int)j48.classifyInstance(current.get(0)));
                    edit.putString("lastClassStr",pred);
                    curr.setCategory(pred);
                    Log.i("Classified data: ",curr.toString());

                    new MyFileWriter().writeToFile(workerLogin+"change",curr.toString()+",Not Sent", BackgroundService.this);
                }

            }catch (Exception e){
                Log.e("Curr classif",e.toString());
            }

            edit.apply();


        }catch (Exception e){
            Log.e("ReadModel",e.toString());
            edit.putString("pred","Brak modelu");
            edit.apply();
        }
    }*/

    public void stopTracking() {

        this.onDestroy();
    }

    private Notification getNotification() {

        NotificationChannel channel = new NotificationChannel("channel_01", "My Channel", NotificationManager.IMPORTANCE_DEFAULT);

        NotificationManager notificationManager = getSystemService(NotificationManager.class);
        notificationManager.createNotificationChannel(channel);

        Notification.Builder builder = new Notification.Builder(getApplicationContext(), "channel_01").setAutoCancel(true);
        return builder.build();
    }

    
    public class LocationServiceBinder extends Binder {
        public BackgroundService getService() {
            return BackgroundService.this;
        }
    }

    public Location getLastLocation(){
        return lastLoc;
    }

    public SensorsData getSensorsData(){
        sensorsData = new SensorsData(lastLoc,mDeviceList,arraylist);
        return sensorsData;
    }






}
