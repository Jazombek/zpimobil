package pl.edu.pwr.zpiclient;

import android.content.Context;
import android.util.Log;


import java.io.IOException;
import java.io.OutputStreamWriter;


import pl.edu.pwr.zpiclient.DataClasses.TrimmedData;

public class MyFileWriter {
    public void writeToFile(String filename, String data, Context context) {
        try {
            Log.e("Write", data);
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(context.openFileOutput(filename+".txt", Context.MODE_APPEND));
            outputStreamWriter.write(data+"\n");
            outputStreamWriter.close();


        }
        catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }

    public void clearFile(String filename, boolean addCsvColumns, Context context) {
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(context.openFileOutput(filename+".txt", Context.MODE_PRIVATE));
            if(addCsvColumns){
                outputStreamWriter.write("Time,Lat,Lon,Alt,Wifi1,Wifi2,Wifi3,Blue1,Blue2,Blue3,Class\n");
                TrimmedData td = new TrimmedData();
                outputStreamWriter.write(td.toString()+"\n");
                Log.i("Empty Data: ",td.toString());
            }
            else {
                outputStreamWriter.write("");
            }
            outputStreamWriter.close();

            Log.i("File Cleared: ",filename);

            //Toast.makeText(WorkerModelActivity.this,"Cleared",Toast.LENGTH_LONG).show();

        }
        catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }
}
