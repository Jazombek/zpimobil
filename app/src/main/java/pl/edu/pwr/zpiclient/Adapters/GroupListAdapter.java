package pl.edu.pwr.zpiclient.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import androidx.constraintlayout.widget.ConstraintLayout;
import pl.edu.pwr.zpiclient.DataClasses.Group;
import pl.edu.pwr.zpiclient.R;
import pl.edu.pwr.zpiclient.Activities.WorkerGroupListActivity;

public class GroupListAdapter extends ArrayAdapter {

    private ArrayList<Group> groups;
    private WorkerGroupListActivity mainActivity;
    private TextView groupNameText,groupDescText;
    private ConstraintLayout cl;

    public GroupListAdapter(Context context, ArrayList<Group> groups) {

        super(context, 0, groups);

        this.groups=groups;
        this.mainActivity=(WorkerGroupListActivity) context;
    }


    @Override
    public int getCount() {
        return groups.size();
    }

    @Override
    public Object getItem(int pos) {
        return groups.get(pos);
    }

    @Override
    public long getItemId(int pos) {
        return 0;
        //just return 0 if your list items do not have an Id variable.
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final Group group = (Group) getItem(position);


        /*if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.song_layout, parent, false);
        }*/
        LayoutInflater inflater = (LayoutInflater) mainActivity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.group_list_layout, null);
        }






        groupNameText = convertView.findViewById(R.id.groupNameText);
        groupDescText = convertView.findViewById(R.id.groupDescText);


        cl=convertView.findViewById(R.id.taskListLayoutBackground);






        groupNameText.setText(group.getName());
        groupDescText.setText(group.getDescription());






        return convertView;
    }
}
