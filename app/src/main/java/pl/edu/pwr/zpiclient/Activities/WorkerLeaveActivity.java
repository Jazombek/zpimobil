package pl.edu.pwr.zpiclient.Activities;

import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import com.google.android.material.navigation.NavigationView;
import androidx.appcompat.app.AppCompatActivity;
import pl.edu.pwr.zpiclient.DataClasses.Worker;
import pl.edu.pwr.zpiclient.R;

import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toast;

public class WorkerLeaveActivity extends AppCompatActivity {
    private Worker worker;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawer_worker_leaves);
        setContents();
        setListeners();

        worker=((Worker)getIntent().getSerializableExtra("worker"));
    }

    public static void start(Context context, Worker worker) {
        Intent starter = new Intent(context, WorkerLeaveActivity.class);
        starter.putExtra("worker",worker);
        context.startActivity(starter);
    }

    private void setContents(){


    }

    private void setListeners(){

    }
}
