package pl.edu.pwr.zpiclient.Activities;

import android.content.Context;
import android.content.Intent;

import androidx.appcompat.app.AppCompatActivity;
import pl.edu.pwr.zpiclient.Adapters.GroupListAdapter;
import pl.edu.pwr.zpiclient.DataClasses.Group;
import pl.edu.pwr.zpiclient.DataClasses.Worker;
import pl.edu.pwr.zpiclient.R;

import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.web.client.RestTemplate;


import java.util.ArrayList;
import java.util.LinkedHashMap;

import static pl.edu.pwr.zpiclient.Activities.MainActivity.apiUri;

public class WorkerGroupListActivity extends AppCompatActivity {

    private Worker worker;
    public static ArrayList<Group> groups=new ArrayList<>();//wszystkie grupy, przynajmniej na razie
    private ListView groupListView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawer_worker_groups);

        getData();
        setContents();
        setListeners();

        worker=((Worker)getIntent().getSerializableExtra("worker"));





    }



    public static void start(Context context,Worker worker) {
        Intent starter = new Intent(context, WorkerGroupListActivity.class);
        starter.putExtra("worker",worker);
        context.startActivity(starter);
    }

    private void getData(){
        RestTemplate restTemplate = RTemplate.getInstance().restTemplate;
        //try{

        ArrayList<LinkedHashMap> response = restTemplate.exchange(apiUri+"groupList", HttpMethod.GET, new HttpEntity<String>(RTemplate.getInstance().sessionHeaders), ArrayList.class).getBody();
        groups = new ArrayList<>();
        for (int i = 0; i < response.size(); i++) {
            LinkedHashMap hm = response.get(i);
            Group g = new Group((int) (hm.get("idGroup")), (String) (hm.get("name")), (String) (hm.get("description")),
                    (int) (hm.get("workerId")));

            //if(worker.getId()==g.getWorkerId()) {
            groups.add(g);
            //}
        }
        Log.i("Groups: ", groups.get(0).getName());
        //Toast.makeText(getApplicationContext(), groups.get(0).getName(), Toast.LENGTH_SHORT).show();
        //}catch (Exception e){}
    }

    private void setContents(){

        groupListView = findViewById(R.id.groupListView);
        GroupListAdapter groupListAdapter = new GroupListAdapter(this,groups);
        groupListView.setAdapter(groupListAdapter);

    }

    private void setListeners(){

    }




}
