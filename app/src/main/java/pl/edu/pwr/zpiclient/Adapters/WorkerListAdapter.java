package pl.edu.pwr.zpiclient.Adapters;

/**
 * Created by bartek on 12.06.2019.
 */

import android.app.Activity;
import android.content.Context;
import androidx.constraintlayout.widget.ConstraintLayout;
import pl.edu.pwr.zpiclient.DataClasses.Worker;
import pl.edu.pwr.zpiclient.R;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by bartek on 24.04.2019.
 */
//zmieniłem na ogólną Activity



public class WorkerListAdapter extends ArrayAdapter {

    private ArrayList workers;
    private Activity mainActivity;
    private TextView workerNameText,workerSurnameText,workerPositionText;

    private ConstraintLayout cl;


    public WorkerListAdapter(Context context, ArrayList<Worker> work) {

        super(context, 0, work);

        this.workers=work;
        this.mainActivity=(Activity) context;
    }


    @Override
    public int getCount() {
        return workers.size();
    }

    @Override
    public Object getItem(int pos) {
        return workers.get(pos);
    }

    @Override
    public long getItemId(int pos) {
        return 0;
        //just return 0 if your list items do not have an Id variable.
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final Worker worker = (Worker)getItem(position);


        /*if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.song_layout, parent, false);
        }*/
        LayoutInflater inflater = (LayoutInflater) mainActivity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.worker_list_layout, null);
        }






        workerNameText = convertView.findViewById(R.id.workerNameListText);
        workerSurnameText = convertView.findViewById(R.id.workerSurnameListText);
        workerPositionText=convertView.findViewById(R.id.workerPositionListText);


        cl=convertView.findViewById(R.id.taskListLayoutBackground);






        workerNameText.setText(worker.getName());
        workerSurnameText.setText(worker.getSurname());
        workerPositionText.setText(worker.getPosition());
        /*if(active) {
            switch (task.getStatus()) {
                case (1):
                    statusButton.setBackgroundColor(Color.GREEN);
                    break;
                case (2):
                    statusButton.setBackgroundColor(Color.RED);
                    break;
                default:
                    //stanowiskoText.setText("Nieznane stanowisko");
                    break;
            }
        }else{
            statusButton.setVisibility(View.INVISIBLE);
        }*/





        return convertView;
    }


}


