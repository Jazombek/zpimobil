package pl.edu.pwr.zpiclient.Activities;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import pl.edu.pwr.zpiclient.DataClasses.Worker;
import pl.edu.pwr.zpiclient.R;
import pl.edu.pwr.zpiclient.Services.BackgroundService;
import pl.edu.pwr.zpiclient.Services.ReadingsTransferService;

import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.widget.Toolbar;

public class WorkerMainActivity extends AppCompatActivity {

    private TextView onlineText;

    private Button taskListBtn, groupListBtn, readingsListBtn;
    private Worker worker;
    private boolean online;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawer_worker_main);
        worker=((Worker)getIntent().getSerializableExtra("worker"));
        online=getIntent().getBooleanExtra("online",false);
        Log.i("WorkerID: ",Long.toString(worker.getId()));
        setContent();
        setListeners();

        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(WorkerMainActivity.this);
        boolean tracking = pref.getBoolean("tracking",false);


        if(tracking){
            startTrackingService();
        }

        if(online){
            startTransferService();
        }


    }
public static void start(Context context, Worker worker, boolean online) {
    Intent starter = new Intent(context, WorkerMainActivity.class);
    starter.putExtra("worker", worker);
    starter.putExtra("online", online);
    context.startActivity(starter);
}
    private void setContent(){

        Toolbar toolbar=findViewById(R.id.workerMainToolbar);
        toolbar.setTitle(worker.getName()+" "+worker.getSurname());
        toolbar.setSubtitle(worker.getPosition());
        setSupportActionBar(toolbar);
        onlineText =findViewById(R.id.onlineText);


        taskListBtn =findViewById(R.id.taskListButton);
        groupListBtn =findViewById(R.id.groupListButton);
        readingsListBtn =findViewById(R.id.readingsButton);




        if(!online){
            taskListBtn.setClickable(false);
            taskListBtn.setBackground(ContextCompat.getDrawable(this,R.drawable.offline_button));

            groupListBtn.setClickable(false);
            groupListBtn.setBackground(ContextCompat.getDrawable(this,R.drawable.offline_button));

            onlineText.setText(getString(R.string.offline_mode));
            ConstraintLayout cl = findViewById(R.id.workerMainConstraintLayout);
            cl.setBackgroundColor(ContextCompat.getColor(this,R.color.lightgray));


        }
        else{
            onlineText.setText(getString(R.string.online_mode));
        }


    }


    private void setListeners(){
        taskListBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(online)
                WorkerTaskListActivity.start(WorkerMainActivity.this,worker);
                else {
                    Toast.makeText(WorkerMainActivity.this,"Niedostępne w trybie offline.",Toast.LENGTH_SHORT).show();
                }
            }
        });

        groupListBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(online)
                WorkerGroupListActivity.start(WorkerMainActivity.this,worker);
                else {
                    Toast.makeText(WorkerMainActivity.this,"Niedostępne w trybie offline.",Toast.LENGTH_SHORT).show();
                }

            }
        });

        readingsListBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                WorkerReadingsActivity.start(WorkerMainActivity.this,worker,online);
            }
        });


    }

    private void startTrackingService(){
        Intent backgroundIntent = new Intent(this, BackgroundService.class);
        backgroundIntent.putExtra("worker",worker.getLogin());
        PendingIntent pintent = PendingIntent.getService(this, 0, backgroundIntent, 0);
        AlarmManager alarm = (AlarmManager)getSystemService(Context.ALARM_SERVICE);

        alarm.setInexactRepeating(AlarmManager.RTC_WAKEUP, 0,60000, pintent);
    }

    private void startTransferService(){
        Intent sendingIntent = new Intent(WorkerMainActivity.this, ReadingsTransferService.class);
        sendingIntent.putExtra("worker",worker.getLogin());
        sendingIntent.putExtra("workerID",Long.toString(worker.getId()));
        sendingIntent.putExtra("online",online);
        PendingIntent pintent = PendingIntent.getService(this, 0, sendingIntent, 0);
        AlarmManager alarm = (AlarmManager)getSystemService(Context.ALARM_SERVICE);

        alarm.setInexactRepeating(AlarmManager.RTC_WAKEUP, 0,6000000, pintent);
    }
}
