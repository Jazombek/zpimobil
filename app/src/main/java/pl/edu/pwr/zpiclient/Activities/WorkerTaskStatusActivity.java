package pl.edu.pwr.zpiclient.Activities;

import android.content.Context;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import pl.edu.pwr.zpiclient.DataClasses.Task;
import pl.edu.pwr.zpiclient.R;

import android.os.Bundle;

public class WorkerTaskStatusActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_worker_task_status);
    }

    public static void start(Context context, Task task) {
        Intent starter = new Intent(context, WorkerTaskStatusActivity.class);
        starter.putExtra("task",task);
        context.startActivity(starter);
    }
}
