package pl.edu.pwr.zpiclient.DataClasses;

import android.location.Location;

import java.util.List;

public class SensorsData {
    public Location loc;
    public List<String> blue;
    public List<String> wifi;

    public SensorsData(Location loc, List<String> blue, List<String> wifi){
        this.loc=loc;
        this.blue=blue;
        this.wifi=wifi;
    }


}
