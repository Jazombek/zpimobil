package pl.edu.pwr.zpiclient.Adapters;

/**
 * Created by bartek on 24.04.2019.
 */


import android.content.Context;

import androidx.constraintlayout.widget.ConstraintLayout;
import pl.edu.pwr.zpiclient.DataClasses.TaskWorker;
import pl.edu.pwr.zpiclient.R;
import pl.edu.pwr.zpiclient.Activities.WorkerTaskListActivity;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;



public class TaskListAdapter extends ArrayAdapter {

    private ArrayList tasks;
    private WorkerTaskListActivity mainActivity;
    private TextView taskNameText, startDateText;
    private ImageButton statusButton;
    private ConstraintLayout cl;


    public TaskListAdapter(Context context, ArrayList<TaskWorker> tasks) {

        super(context, 0, tasks);

        this.tasks=tasks;
        this.mainActivity=(WorkerTaskListActivity) context;

    }


    @Override
    public int getCount() {
        return tasks.size();
    }

    @Override
    public Object getItem(int pos) {
        return tasks.get(pos);
    }

    @Override
    public long getItemId(int pos) {
        return 0;
        //just return 0 if your list items do not have an Id variable.
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final TaskWorker task = (TaskWorker)getItem(position);


        /*if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.song_layout, parent, false);
        }*/
        LayoutInflater inflater = (LayoutInflater) mainActivity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.task_list_layout, null);
        }

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mainActivity.taskDetails(task);
            }
        });




        taskNameText = convertView.findViewById(R.id.taskNameText);
        startDateText = convertView.findViewById(R.id.startDateText);
        statusButton = convertView.findViewById(R.id.taskStatusButton);
        if(task.getIdStatus()==2){
            statusButton.setVisibility(View.INVISIBLE);
        }
        if(task.getIdWorkerStatus()==5)
            statusButton.setImageResource(R.drawable.ic_play);
        else{
            statusButton.setImageResource(R.drawable.ic_pause);

        }

        statusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int id = task.getIdWorkerStatus();
                if(id!=4) {

                    ((ImageButton)view).setImageResource(R.drawable.ic_pause);
                    task.setIdWorkerStatus(4);
                    mainActivity.taskStatus(task);
                }
                else{
                    ((ImageButton)view).setImageResource(R.drawable.ic_play);
                    task.setIdWorkerStatus(5);
                    mainActivity.taskStatus(task);
                }

                mainActivity.taskStatus(task);
            }
        });

        cl=convertView.findViewById(R.id.taskListLayoutBackground);






        taskNameText.setText(task.getName());
        startDateText.setText(task.getData());
        /*if(active) {
            switch (task.getStatus()) {
                case (1):
                    statusButton.setBackgroundColor(Color.GREEN);
                    break;
                case (2):
                    statusButton.setBackgroundColor(Color.RED);
                    break;
                default:
                    //stanowiskoText.setText("Nieznane stanowisko");
                    break;
            }
        }else{
            statusButton.setVisibility(View.INVISIBLE);
        }*/





        return convertView;
    }

    /*private void switchIcons(View view){
        ImageButton btn=view.findViewById(R.id.taskStatusButton);
        if(active){
            btn.setImageResource(R.drawable.ic_pause);
        }
        else{
            btn.setImageResource(R.drawable.ic_play);
        }
        active=!active;
    }*/


}


