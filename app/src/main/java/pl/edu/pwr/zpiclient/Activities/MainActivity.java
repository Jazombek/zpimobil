package pl.edu.pwr.zpiclient.Activities;

import android.app.FragmentManager;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import pl.edu.pwr.zpiclient.DataClasses.Worker;
import pl.edu.pwr.zpiclient.MyFileWriter;
import pl.edu.pwr.zpiclient.R;
import pl.edu.pwr.zpiclient.SSLUtil;

import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.ArrayList;
import java.util.LinkedHashMap;

public class MainActivity extends AppCompatActivity {

    public static String apiUri="https://192.168.1.12:8434/api/";
    private TextView urlText,loginText,passText;
    private Button offlineButton;
    private RestTemplate rest = new RestTemplate();
    private FragmentManager fm;
    public static Worker thisWorker=null;//tutaj są dane naszego workera w tym id
    private SharedPreferences pref;
    private SharedPreferences.Editor edit ;
    private boolean rememberLogin = true;
    private Worker worker;
    private String body;
    private CheckBox rememberBox;




    //private String url = "http://localhost:8090/api/";
    //z emulatora 10.0.2.2
    //private String url = "http://10.0.2.2:8090/";
    void getUserId(String login){
        RestTemplate restTemplate = RTemplate.getInstance().restTemplate;
        try{

        ArrayList<LinkedHashMap> response = restTemplate.exchange(urlText.getText()+"workerList", HttpMethod.GET, new HttpEntity<String>(RTemplate.getInstance().sessionHeaders), ArrayList.class).getBody();



        Log.i("Workers: ", Integer.toString(response.size()));
        for (int i = 0; i < response.size(); i++) {
            LinkedHashMap hm = (LinkedHashMap) response.get(i);
            if(login.equals((String)(hm.get("login")))){
                thisWorker=new Worker((int)(hm.get("id")),(String)(hm.get("name")),(String)(hm.get("surname")),(String)(hm.get("login")),
                        (String)(hm.get("password")), (String)(hm.get("position")), (int)(hm.get("idStatus")));

            }


        }
        }catch (Exception e){
            Toast.makeText(getApplicationContext(), e.getMessage().toString(),Toast.LENGTH_SHORT).show();
        }
    }


    Runnable login = new Runnable() {
        @Override
        public void run() {
            try {

                final String uri = urlText.getText() + "login";

                RestTemplate restTemplate = RTemplate.getInstance().restTemplate;
                //HttpComponentsClientHttpRequestFactory rf =
                //        (HttpComponentsClientHttpRequestFactory) restTemplate.getRequestFactory();
                //rf.setReadTimeout(5000);
                //rf.setConnectTimeout(5000);

                //RestTemplate restTemplate = new RestTemplate(new HttpComponentsClientHttpRequestFactory());


                HttpHeaders headers = new HttpHeaders();
                headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

                MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
                //map.add("username", w.name);
                //map.add("password", w.password);
                map.add("username", worker.getLogin());
                map.add("password", worker.getPassword());

                HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);

                SSLUtil.turnOffSslChecking();
                //String tmp = restTemplate.postForObject(uri, request, String.class);
                ResponseEntity<String> tmp = restTemplate.exchange(uri, HttpMethod.POST, request, String.class);
                String cookie = tmp.getHeaders().get("Set-Cookie").get(0);
                HttpHeaders requestHeaders = new HttpHeaders();
                requestHeaders.add("Cookie", cookie);
                RTemplate.getInstance().sessionHeaders = requestHeaders;

                String response = tmp.getBody();

                if ("loginSuccess".equals(response)) {
                    getUserId(worker.getLogin());
                    if (rememberBox.isChecked()) {
                        edit.putBoolean("rememberLogin", rememberBox.isChecked());
                        edit.putString("lastLogin", loginText.getText().toString());
                        edit.putString("lastPassword", passText.getText().toString());
                        edit.putString("lastUrl",urlText.getText().toString());
                        Gson gson = new Gson();
                        String json = gson.toJson(thisWorker);
                        edit.putString(loginText.getText().toString(), json);
                        edit.apply();
                    } else {
                        edit.putString("lastLogin", "");
                        edit.putString("lastPassword", "");
                        edit.apply();
                    }

                    WorkerMainActivity.start(MainActivity.this, thisWorker,true);


                }

            } catch (Exception e) {
                Log.i("Login error: ",e.toString());
                //Toast.makeText(getApplicationContext(), "Niepoprawny adres serwera", Toast.LENGTH_LONG).show();
            }
        }
    };
    /*String login(Worker w){
        try {
            //final String uri = "https://10.0.2.2:8434/api/login";
            //final String uri = "https://192.168.0.73:8434/api/login";
            final String uri=urlText.getText()+"login";

            RestTemplate restTemplate = RTemplate.getInstance().restTemplate;
            //HttpComponentsClientHttpRequestFactory rf =
            //        (HttpComponentsClientHttpRequestFactory) restTemplate.getRequestFactory();
            //rf.setReadTimeout(5000);
            //rf.setConnectTimeout(5000);

            //RestTemplate restTemplate = new RestTemplate(new HttpComponentsClientHttpRequestFactory());


            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

            MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
            //map.add("username", w.name);
            //map.add("password", w.password);
            map.add("username", w.login);
            map.add("password", w.password);

            HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);

            SSLUtil.turnOffSslChecking();
            //String tmp = restTemplate.postForObject(uri, request, String.class);
            ResponseEntity<String> tmp = restTemplate.exchange(uri, HttpMethod.POST, request, String.class);
            String cookie = tmp.getHeaders().get("Set-Cookie").get(0);
            HttpHeaders requestHeaders = new HttpHeaders();
            requestHeaders.add("Cookie", cookie);
            RTemplate.getInstance().sessionHeaders = requestHeaders;

            return tmp.getBody();



        } catch (Exception e) {
            //Toast.makeText(getApplicationContext(), "Niepoprawny adres serwera", Toast.LENGTH_LONG).show();
        }
        return null;
    }*/




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

        StrictMode.setThreadPolicy(policy);


        setContentView(R.layout.activity_main);
        Toolbar toolbar =  findViewById(R.id.toolbar);
        //toolbar.setTitle("Login");
        //toolbar.setTitleTextColor(ContextCompat.getColor(this,R.color.colorAccent));
        //toolbar.setDrawingCacheBackgroundColor(ContextCompat.getColor(this,R.color.background));
        setSupportActionBar(toolbar);

        fm=getFragmentManager();

        pref = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
        edit = pref.edit();

        final ImageButton loginButton = findViewById(R.id.LoginButton);
        loginText = findViewById(R.id.LoginText);
        passText = findViewById(R.id.PassText);
        rememberBox = findViewById(R.id.rememberCheckBox);
        urlText = findViewById(R.id.urlText);
        offlineButton = findViewById(R.id.offlineButton);

        urlText.setText(pref.getString("lastUrl",apiUri));


        urlText.setVisibility(View.INVISIBLE);

        rememberLogin = pref.getBoolean("rememberLogin", true);
        rememberBox.setChecked(rememberLogin);

        if(rememberLogin){
            loginText.setText(pref.getString("lastLogin", ""));
            passText.setText(pref.getString("lastPassword", ""));
            //worker1
            //Toast.makeText(MainActivity.this,pref.getString("lastPassword", ""),Toast.LENGTH_SHORT).show();
        }
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //loginText.setText("");
                worker = new Worker();
                worker.setLogin(loginText.getText().toString());
                worker.setPassword(passText.getText().toString());

                apiUri=urlText.getText().toString();

                Toast.makeText(MainActivity.this,"Logowanie",Toast.LENGTH_SHORT).show();
                AsyncTask.execute(check);

                AsyncTask.execute(login);




                /*if("loginSuccess".equals(response)){
                    getUserId(temp.login);
                    if(rememberBox.isChecked()){
                        edit.putBoolean("rememberLogin", rememberBox.isChecked());
                        edit.putString("lastLogin", loginText.getText().toString());
                        edit.putString("lastPassword", passwordText.getText().toString());
                        Gson gson = new Gson();
                        String json = gson.toJson(thisWorker);
                        edit.putString(loginText.getText().toString(), json);
                        edit.apply();
                    }
                    else {
                        edit.putString("lastLogin", "");
                        edit.putString("lastPassword", "");
                        edit.apply();
                    }

                    WorkerMainActivity.start(MainActivity.this, thisWorker);
                }else{
                    Toast.makeText(MainActivity.this, "Niepoprawne dane logowania.", Toast.LENGTH_SHORT).show();
                }*/

                //Toast.makeText(MainActivity.this, RTemplate.getInstance().sessionHeaders.get("Cookie").toString(), Toast.LENGTH_SHORT);
                //RestTemplate restTemplate = RTemplate.getInstance().restTemplate;


                    /*try {
                        String response = (String) new Http_findLogin_Task().execute().get();
                        //loginText.setText(((User) response).toString())
                        if("loginSuccess".equals(response)){
                            startActivity(new Intent(MainActivity.this, WorkerMainActivity.class));
                        }else{
                            Toast.makeText(MainActivity.this, "Niepoprawne dane logowania.", Toast.LENGTH_SHORT).show();
                        }
                    } catch (InterruptedException e) {

                        //e.printStackTrace();

                    } catch (ExecutionException e) {

                        //e.printStackTrace();

                    }*/
            }
        });
        offlineButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //TODO to jest do prezentacji
                Worker temp = new Worker(3,"Karol","Kowalski","work2","worker2", "Malarz", 5);
                edit.putString("lastClassStr","Brak");
                edit.apply();
                MyFileWriter mfw = new MyFileWriter();
                String[] prez=new String[14];
                prez[0]="2020-05-30 16:06:30,50.693993,17.751772,198.60089,Funbox@Jozkow,null,null,null,null,null,Praca";
                prez[1]="2020-05-30 16:06:30,50.693995,17.751771,198.60089,Funbox@Jozkow,null,null,null,null,null,Praca";
                prez[2]="2020-05-30 16:06:30,50.693993,17.751772,198.60089,null,null,null,null,null,null,Nie Praca";
                prez[3]="2020-05-30 16:06:30,50.693997,17.751769,198.60089,null,null,null,null,null,null,Nie Praca";
                prez[4]="2020-05-30 16:06:30,50.693990,17.751777,198.60089,Funbox@Jozkow,null,null,null,null,null,Praca";
                prez[5]="2020-05-30 16:06:30,50.693993,17.751778,198.60089,null,null,null,null,null,null,Nie Praca";
                prez[6]="2020-05-30 16:06:30,50.693969,17.751769,198.60089,Funbox@Jozkow,null,null,null,null,null,Praca";
                prez[7]="2020-05-30 16:06:30,50.693993,17.751772,198.60089,Funbox@Jozkow,null,null,null,null,null,Praca";
                prez[8]="2020-05-30 16:06:30,50.693995,17.751771,198.60089,Funbox@Jozkow,null,null,null,null,null,Praca";
                prez[9]="2020-05-30 16:06:30,50.693993,17.751772,198.60089,null,null,null,null,null,null,Nie Praca";
                prez[10]="2020-05-30 16:06:30,50.693997,17.751769,198.60089,null,null,null,null,null,null,Praca";
                prez[11]="2020-05-30 16:06:30,50.693969,17.751769,198.60089,5Funbox@Jozkow,null,null,null,null,null,Praca";
                prez[12]="2020-05-30 16:06:30,50.693993,17.751772,198.60089,5Funbox@Jozkow,null,null,null,null,null,Praca";
                prez[13]="2020-05-30 16:06:30,50.693995,17.751771,198.60089,5Funbox@Jozkow,null,null,null,null,null,Praca";
                mfw.clearFile(temp.getLogin()+"j48",false,MainActivity.this);
                mfw.clearFile(temp.getLogin()+"data",true,MainActivity.this);
                mfw.clearFile(temp.getLogin()+"change",false,MainActivity.this);
                mfw.writeToFile(temp.getLogin()+"data",prez[0],MainActivity.this);
                mfw.writeToFile(temp.getLogin()+"data",prez[1],MainActivity.this);
                mfw.writeToFile(temp.getLogin()+"data",prez[2],MainActivity.this);
                mfw.writeToFile(temp.getLogin()+"data",prez[3],MainActivity.this);
                mfw.writeToFile(temp.getLogin()+"data",prez[4],MainActivity.this);
                mfw.writeToFile(temp.getLogin()+"data",prez[5],MainActivity.this);
                mfw.writeToFile(temp.getLogin()+"data",prez[6],MainActivity.this);
                mfw.writeToFile(temp.getLogin()+"data",prez[7],MainActivity.this);
                mfw.writeToFile(temp.getLogin()+"data",prez[8],MainActivity.this);
                mfw.writeToFile(temp.getLogin()+"data",prez[9],MainActivity.this);
                mfw.writeToFile(temp.getLogin()+"data",prez[10],MainActivity.this);
                WorkerMainActivity.start(MainActivity.this, temp,false);
            }
        });

    }


    @Override
    public boolean onCreateOptionsMenu (Menu menu){
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected (MenuItem item){
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            urlText.setVisibility(View.VISIBLE);
            offlineButton.setVisibility(View.VISIBLE);




        }

        return super.onOptionsItemSelected(item);
    }


    /*private class Http_findLogin_Task extends AsyncTask  {

        @Override
        protected Object doInBackground(Object[] objects) {

            try {
                //final String uri = "https://10.0.2.2:8434/api/login";
                //final String uri = "https://192.168.0.73:8434/api/login";
                final String uri = apiUri+"login";
                RestTemplate restTemplate = RTemplate.getInstance().restTemplate;
                //RestTemplate restTemplate = new RestTemplate(new HttpComponentsClientHttpRequestFactory());


                HttpHeaders headers = new HttpHeaders();
                headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

                MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
                map.add("username", "work");
                map.add("password", "worker1");

                HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);

                SSLUtil.turnOffSslChecking();
                //String tmp = restTemplate.postForObject(uri, request, String.class);
                ResponseEntity<String> tmp = restTemplate.exchange(uri, HttpMethod.POST, request, String.class);
                String cookie = tmp.getHeaders().get("Set-Cookie").get(0);
                HttpHeaders requestHeaders = new HttpHeaders();
                requestHeaders.add("Cookie", cookie);
                RTemplate.getInstance().sessionHeaders = requestHeaders;



                return tmp.getBody();


            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_SHORT).show();
            }
            return null;
        }
    }*/

    Runnable check = new Runnable() {
        @Override
        public void run() {
            try {
                URL url = new URL(apiUri);

                SSLUtil.turnOffSslChecking();
                HttpURLConnection urlc = (HttpURLConnection) url.openConnection();
                urlc.setRequestProperty("User-Agent", "Android Application:");
                urlc.setRequestProperty("Connection", "close");
                urlc.setConnectTimeout(1000 * 5); // mTimeout is in seconds
                urlc.connect();

                if (urlc.getResponseCode() == 200) {
                    //Toast.makeText(MainActivity.this,"Conn",Toast.LENGTH_SHORT).show();
                    Log.i("Ping", "Success");
                }


            }catch (SocketTimeoutException ste){
                Gson gson = new Gson();
                String json = pref.getString(loginText.getText().toString(),"");
                if(!json.equals("")&&rememberLogin) {
                    Worker offline = gson.fromJson(json, Worker.class);
                    WorkerMainActivity.start(MainActivity.this, offline,false);

                }

                Log.i("Ping", ste.toString());

            }
            catch (Exception e1) {
                Gson gson = new Gson();
                String json = pref.getString(loginText.getText().toString(),"");
                if(!json.equals("")&&rememberLogin) {
                    Worker offline = gson.fromJson(json, Worker.class);
                    WorkerMainActivity.start(MainActivity.this, offline,false);

                }                Log.e("Ping", e1.toString());
            }
        }
    };



}
