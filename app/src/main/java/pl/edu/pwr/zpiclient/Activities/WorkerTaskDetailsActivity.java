package pl.edu.pwr.zpiclient.Activities;

import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import androidx.fragment.app.DialogFragment;
import androidx.appcompat.app.AppCompatActivity;
import pl.edu.pwr.zpiclient.Adapters.ResourceListAdapter;
import pl.edu.pwr.zpiclient.Adapters.WorkerListAdapter;
import pl.edu.pwr.zpiclient.DataClasses.Resource;
import pl.edu.pwr.zpiclient.DataClasses.TaskWorker;
import pl.edu.pwr.zpiclient.DataClasses.Worker;
import pl.edu.pwr.zpiclient.FinishDialog;
import pl.edu.pwr.zpiclient.R;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import static pl.edu.pwr.zpiclient.Activities.MainActivity.apiUri;

public class WorkerTaskDetailsActivity extends AppCompatActivity implements FinishDialog.FinishDialogListener {
    ArrayList<Resource>resources=new ArrayList<>();//lista wszystkich surowców z przypisanymi idtask
    ArrayList<Worker> workers=new ArrayList<>();
    private TaskWorker task;
    private Worker worker;
    private Button reportB,statusB, finishB;
    private ImageButton statusImB;
    private ListView resourcesL,workersL;
    //private FragmentManager fm = getFragmentManager();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_worker_task_details);
        task=((TaskWorker)getIntent().getSerializableExtra("task"));
        worker=((Worker)getIntent().getSerializableExtra("worker"));
        setTexts();
        setListeners();

        getData();

        setResourcesList();
    }
    public static void start(Context context,TaskWorker task,Worker worker) {
        Intent starter = new Intent(context, WorkerTaskDetailsActivity.class);
        starter.putExtra("task",task);
        starter.putExtra("worker",worker);
        context.startActivity(starter);
    }

    private void setTexts(){
        TextView taskName = findViewById(R.id.taskNameText);
        TextView taskStart = findViewById(R.id.taskStartDateText);
        TextView taskEnd = findViewById(R.id.taskEndDateText);
        TextView taskDescription = findViewById(R.id.descriptionText);
        statusB = findViewById(R.id.changeStatusButton);
        statusImB = findViewById(R.id.taskStatusButton);
        finishB=findViewById(R.id.finishTaskButton);
        reportB=findViewById(R.id.sendReportButton);
        resourcesL = findViewById(R.id.resourcesListView);
        workersL=findViewById(R.id.workersListView);

        taskDescription.setText(task.getDescription());


        taskName.setText(task.getName());
        taskStart.setText(task.getData());
        taskEnd.setText(task.getData());


        int status = task.getIdWorkerStatus();
        if(status==5) {

            statusImB.setImageResource(R.drawable.ic_play);

        }
        else{
            statusImB.setImageResource(R.drawable.ic_pause);

        }


    }

    private void setListeners(){
        reportB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                WorkerTaskReportActivity.start(WorkerTaskDetailsActivity.this,task,resources,worker);
            }
        });

        finishB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finishTask();
                //finish();
            }
        });

        statusImB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeStatus();
            }
        });

        statusB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeStatus();
            }
        });


    }

    private void changeStatus(){
        int status = task.getIdWorkerStatus();
        if(status!=4) {

            statusImB.setImageResource(R.drawable.ic_pause);
            task.setIdWorkerStatus(4);

        }
        else{
            statusImB.setImageResource(R.drawable.ic_play);
            task.setIdWorkerStatus(5);

        }
        RestTemplate restTemplate = RTemplate.getInstance().restTemplate;
        HttpHeaders headers=RTemplate.getInstance().sessionHeaders;
        headers.add("idW", String.valueOf(worker.getId()));
        headers.add("idS", String.valueOf(task.getIdWorkerStatus()));
        headers.add("idT", String.valueOf(task.getIdTask()));
        HttpEntity request=new HttpEntity<String>(headers);

        restTemplate.exchange(apiUri + "updateWTS", HttpMethod.GET, request, String.class).getBody();
        //Toast.makeText(getApplicationContext(), response,Toast.LENGTH_SHORT).show();
        headers.remove("idW");
        headers.remove("idS");
        headers.remove("idT");


    }

    private void setResourcesList(){
        /*
        resources.add(new Resource(34,"Deski","Bukowe"));
        resources.add(new Resource(35,"Piła","Zębata"));
        resources.add(new Resource(36,"Gwoździe","Ostre"));

        workers.add(new Worker(67,"Franek","Kotara","asdf","fdsa2", "Drwal", 4));*/
        ResourceListAdapter resAdap = new ResourceListAdapter(this, resources);
        //workers.add(new Worker(31,"ad","sd","re","wr","pos",32));
        WorkerListAdapter worAdap=new WorkerListAdapter(this, workers);



        resourcesL.setAdapter(resAdap);
        workersL.setAdapter(worAdap);
    }

    private void getData(){
        RestTemplate restTemplate = RTemplate.getInstance().restTemplate;
        workers.clear();
        resources.clear();
        //try {
            ArrayList<LinkedHashMap> response = restTemplate.exchange(apiUri + "resList", HttpMethod.GET, new HttpEntity<String>(RTemplate.getInstance().sessionHeaders), ArrayList.class).getBody();

            for (int i = 0; i < response.size(); i++) {
                LinkedHashMap hm =  response.get(i);
                Resource r = new Resource((int) (hm.get("idResource")), (String) (hm.get("name")), (String) (hm.get("description")),
                        (int) (hm.get("idTask")));
                //Toast.makeText(WorkerTaskDetailsActivity.this, r.getName().toString(),Toast.LENGTH_SHORT).show();
                if(r.getIdTask()==task.getIdTask())
                    resources.add(r);
            }

            ArrayList<LinkedHashMap> response2 = restTemplate.exchange(apiUri + "taskWorker/"+task.getIdTask(), HttpMethod.GET, new HttpEntity<String>(RTemplate.getInstance().sessionHeaders), ArrayList.class).getBody();
            //Toast.makeText(WorkerTaskDetailsActivity.this,String.valueOf(response2.size()),Toast.LENGTH_SHORT).show();

            //Toast.makeText(WorkerTaskDetailsActivity.this,test.get("id").toString(),Toast.LENGTH_SHORT).show();
            for (int j = 0; j < response2.size(); j++) {
                LinkedHashMap hm =  response2.get(j);
                //Toast.makeText(WorkerTaskDetailsActivity.this, (hm.get("id").toString()),Toast.LENGTH_SHORT).show();
                Worker w = new Worker((int) (hm.get("id")), (String) (hm.get("name")), (String) (hm.get("surname")), (String) (hm.get("login")),
                        (String)(hm.get("password")),(String)(hm.get("position")),
                        (int) (hm.get("idStatus")));
               // Toast.makeText(WorkerTaskDetailsActivity.this, w.getName(),Toast.LENGTH_SHORT).show();

                workers.add(w);
            }

        //}catch(Exception e){}
    }
    private void finishTask(){
        DialogFragment dialog = new FinishDialog();
        dialog.show(getSupportFragmentManager(), "FinishDialog");
    }

    @Override
    public void onDialogNegativeClick(DialogFragment dialog) {

    }

    @Override
    public void onDialogPositiveClick(DialogFragment dialog) {
        finishTask2();
    }

    private void finishTask2(){
        RestTemplate restTemplate = RTemplate.getInstance().restTemplate;
        HttpHeaders headers=RTemplate.getInstance().sessionHeaders;

        headers.add("idS", "3");
        headers.add("idT", String.valueOf(task.getIdTask()));
        HttpEntity request=new HttpEntity<String>(headers);

        restTemplate.exchange(apiUri + "updateTaskS2", HttpMethod.GET, request, String.class).getBody();
        //Toast.makeText(getApplicationContext(), response,Toast.LENGTH_SHORT).show();

        headers.remove("idS");
        headers.remove("idT");


        finish();


    }
}
