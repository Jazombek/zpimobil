package pl.edu.pwr.zpiclient.Services;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.IBinder;
import android.util.Log;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.web.client.RestTemplate;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import androidx.annotation.Nullable;
import pl.edu.pwr.zpiclient.Activities.RTemplate;
import pl.edu.pwr.zpiclient.DataClasses.TrimmedData;
import pl.edu.pwr.zpiclient.MyFileWriter;
import pl.edu.pwr.zpiclient.SSLUtil;

import static pl.edu.pwr.zpiclient.Activities.MainActivity.apiUri;

public class ReadingsTransferService extends Service {

    private ArrayList<TrimmedData> readings = new ArrayList<>();
    private String workerID,workerLogin;
    private boolean changeFlag=false;
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId){

        workerLogin = intent.getStringExtra("worker");
        workerID = intent.getStringExtra("workerID");
        boolean online = intent.getBooleanExtra("online",false);

        Log.i("TransferServiceStarted: ",new TrimmedData().getStringDate());

        readings.clear();


        try (BufferedReader br = new BufferedReader(new InputStreamReader(openFileInput(workerLogin+"change.txt")))) {
            String line;
            //line = br.readLine();
            while ((line = br.readLine()) != null) {
                Log.i("Line Read:",line);

                TrimmedData reading = new TrimmedData(line);
                readings.add(reading);

            }
        }catch (Exception e){
            Log.e("Error reading data",e.toString());
        }

        Log.i("Readings Size",readings.size()+"");

        if(online){
            sendData();





        }
        return Service.START_NOT_STICKY;
    }

    private void sendData() {

        Runnable check = new Runnable() {
            @Override
            public void run() {
                try {
                    URL url = new URL(apiUri);

                    SSLUtil.turnOffSslChecking();
                    HttpURLConnection urlc = (HttpURLConnection) url.openConnection();
                    urlc.setRequestProperty("User-Agent", "Android Application:");
                    urlc.setRequestProperty("Connection", "close");
                    urlc.setConnectTimeout(1000 * 5); // mTimeout is in seconds
                    urlc.connect();

                    if (urlc.getResponseCode() == 200) {
                        beginTransfer();

                        trimChange();
                    }


                } catch (Exception e1) {
                    Log.e("Ping", e1.toString());
                }
            }
        };

        AsyncTask.execute(check);
    }

    private void beginTransfer(){
        RestTemplate restTemplate = RTemplate.getInstance().getRestTemplate();
        HttpHeaders headers=RTemplate.getInstance().getSessionHeaders();

        TrimmedData singleDataPoint;

        int i=0;

        while(i<readings.size()) {
            singleDataPoint = readings.get(i);
            //Log.i("I",i+"");
            //Log.i("Readings Size",readings.size()+"");
            i++;

            if(singleDataPoint.getStatus().equals("Not Sent")) {

                changeFlag=true;


                //Log.i("Datapoint Read: ",singleDataPoint.toTrimmedString());
                //Log.i("wid",workerID);
                headers.add("wid", workerID);
                headers.add("time", singleDataPoint.getStringDate());
                headers.add("f1", Float.toString(singleDataPoint.getLat()));
                headers.add("f2", Float.toString(singleDataPoint.getLon()));
                headers.add("f3", Float.toString(singleDataPoint.getAlt()));

                String[] strings = singleDataPoint.getStrings();
                headers.add("s1", strings[0]);
                headers.add("s2", strings[1]);
                headers.add("s3", strings[2]);
                headers.add("s4", strings[3]);
                headers.add("s5", strings[4]);
                headers.add("s6", strings[5]);
                headers.add("s7", strings[6]);
                headers.add("s8", "none");
                headers.add("s9", "none");
                headers.add("s10", "none");
                headers.add("s11", "none");
                headers.add("s12", "none");
                headers.add("s13", "none");
                headers.add("s14", "none");
                headers.add("s15", "none");
                headers.add("s16", "none");
                headers.add("s17", "none");
                headers.add("s18", "none");
                headers.add("s19", "none");
                headers.add("s20", "none");

        /*headers.add("wid", workerID);
        headers.add("time", new TrimmedData().getStringDate());
        headers.add("f1", "1");
        headers.add("f2", "2");
        headers.add("f3", "3");

        //String[] strings=singleDataPoint.getStrings();
        headers.add("s1","a");
        headers.add("s2","b");
        headers.add("s3","c");
        headers.add("s4","d");
        headers.add("s5","e");
        headers.add("s6","f");
        headers.add("s7","g");
        headers.add("s8","h");
        headers.add("s9","i");
        headers.add("s10","j");
        headers.add("s11","k");
        headers.add("s12","l");
        headers.add("s13","m");
        headers.add("s14","n");
        headers.add("s15","o");
        headers.add("s16","p");
        headers.add("s17","q");
        headers.add("s18","r");
        headers.add("s19","s");
        headers.add("s20","t");


        headers.add("bool","true");*/
                HttpEntity request = new HttpEntity<String>(headers);

                restTemplate.exchange(apiUri + "putTimestamp", HttpMethod.GET, request, String.class).getBody();
                //Toast.makeText(getApplicationContext(), response,Toast.LENGTH_SHORT).show();

                headers.remove("wid");
                headers.remove("time");
                headers.remove("f1");
                headers.remove("f2");
                headers.remove("f3");
                headers.remove("bool");
                headers.remove("s1");
                headers.remove("s2");
                headers.remove("s3");
                headers.remove("s4");
                headers.remove("s5");
                headers.remove("s6");
                headers.remove("s7");
                headers.remove("s8");
                headers.remove("s9");
                headers.remove("s10");
                headers.remove("s11");
                headers.remove("s12");
                headers.remove("s13");
                headers.remove("s14");
                headers.remove("s15");
                headers.remove("s16");
                headers.remove("s17");
                headers.remove("s18");
                headers.remove("s19");
                headers.remove("s20");
            }
        }



    }

    private void trimChange(){
        if(changeFlag) {
            changeFlag=false;
            MyFileWriter mfw = new MyFileWriter();
            mfw.clearFile(workerLogin + "change", false, ReadingsTransferService.this);

            int i = readings.size() - 10;
            if (i < 0)
                i = 0;
            while (i < readings.size()) {
                TrimmedData temp = readings.get(i);
                temp.setStatus("Sent");
                //Log.i("Temp: ", temp.toStatusSting());

                i++;


                mfw.writeToFile(workerLogin + "change", temp.toStatusSting(), ReadingsTransferService.this);
            }
        }
    }



}
