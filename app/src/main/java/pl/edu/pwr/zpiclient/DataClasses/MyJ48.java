package pl.edu.pwr.zpiclient.DataClasses;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import java.io.File;
import java.io.FileWriter;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Calendar;
import java.util.Date;

import pl.edu.pwr.zpiclient.MyFileWriter;
import pl.edu.pwr.zpiclient.Services.BackgroundService;
import weka.classifiers.trees.J48;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.CSVLoader;

public class MyJ48 {
    private J48 j48;
    private Instances data;
    private String workerLogin;

    public MyJ48 (String workerLogin){
        this.workerLogin=workerLogin;

    }

    public Instances readData(String filename){



        return data;
    }

    public TrimmedData predict (Context context, TrimmedData trimmedData){
        String predictString = "";
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor edit=pref.edit();
        CSVLoader loader = new CSVLoader();
        try {


            //Log.i("model",j48.toString());

            File temp=File.createTempFile(workerLogin+"curr","txt", context.getCacheDir());
            //FileOutputStream fos = new FileOutputStream(temp);
            FileWriter fw = new FileWriter(temp,true);
            fw.write("Time,Lat,Lon,Alt,Wifi1,Wifi2,Wifi3,Blue1,Blue2,Blue3,Class\n");
            //FileCopyUtils.copy(openFileInput(workerLogin+"data.txt"),fos);
            //wifiArray[0]="Nowa";
            //blueArray=new String[3];
            //blueArray[0]="Przykład";

            //TrimmedData curr=new TrimmedData(new Date(Calendar.getInstance().getTimeInMillis()),lat,lon,alt,wifiArray,blueArray,"Brak");


            //Instances current=loader.getDataSet();


            loader.setSource(context.openFileInput(workerLogin+"data.txt"));

            Instances train=loader.getDataSet();

            String[] strings =trimmedData.getStrings();

            try {

                if (train.attribute("Wifi1").indexOfValue(strings[0]) == -1) {
                    strings[0] = "null";
                }
                if (train.attribute("Wifi2").indexOfValue(strings[1]) == -1) {
                    strings[1] = "null";
                }
                if (train.attribute("Wifi3").indexOfValue(strings[2]) == -1) {
                    strings[2] = "null";
                }

                if (train.attribute("Blue1").indexOfValue(strings[3]) == -1) {
                    strings[3] = "null";
                }
                if (train.attribute("Blue2").indexOfValue(strings[4]) == -1) {
                    strings[4] = "null";
                }
                if (train.attribute("Blue3").indexOfValue(strings[5]) == -1) {
                    strings[5] = "null";
                }
            }catch (Exception e){
                Log.e("Error standarizing unknown parameters",e.toString());
            }
            //TrimmedData tempData=new TrimmedData(new Date(Calendar.getInstance().getTimeInMillis()),lat,lon,alt,wifiArray,blueArray,"Brak");



            fw.write(trimmedData.toString());
            //fw.write(lat+","+lon+","+alt+","+wifiArray[0]+","+wifiArray[1]+","+wifiArray[2]+","+blueArray[0]+","+blueArray[1]+","+blueArray[2]+","+"none");
            fw.flush();
            fw.close();


            loader.setSource(temp);
            Instances current=loader.getDataSet();
            //current.deleteAttributeAt(0);
            current.setClassIndex(current.numAttributes()-1);
            //Log.i("Curr: ",current.toString());
            //Instance currInstance = current.instance(0);




            train.add(current.instance(0));

            try {
                train.instance(train.size() - 1).setValue(4, strings[0]);
                train.instance(train.size() - 1).setValue(5, strings[1]);
                train.instance(train.size() - 1).setValue(6, strings[2]);

                train.instance(train.size() - 1).setValue(7, strings[3]);
                train.instance(train.size() - 1).setValue(8, strings[4]);
                train.instance(train.size() - 1).setValue(9, strings[5]);
            }catch (Exception e){
                Log.e("Error assigning standarized parameters ",e.toString());
            }

            //Log.i("Instance:",current.instance(0).toString(4));


            train.deleteAttributeAt(0);
            train.setClassIndex(train.numAttributes()-1);

            //Instances labeled = new Instances(train);

            //train.instance(train.size()-1).setValue(3,"null");


            //int classif = pref.getInt("lastClass",-1);


            Log.i("Current data:",train.instance(train.size()-1).toString());
            try {

                double classVal=j48.classifyInstance(train.instance(train.size()-1));
                Log.i("double val:",Double.toString(classVal));

                //labeled.instance(train.size()-1).setClassValue(classVal);
                predictString = train.classAttribute().value((int)classVal);

                edit.putString("pred",predictString);
                trimmedData.setCategory(predictString);
                Log.i("Classified data: ",trimmedData.toString());


            }catch (Exception e){
                edit.putString("pred", "Brak modelu");
                Log.e("Curr classif",e.toString());
            }

            edit.apply();


        }catch (Exception e){
            Log.e("ReadModel",e.toString());
            edit.putString("pred","Brak modelu");
            edit.apply();
        }
        return trimmedData;
    }

    public J48 createModel(Context context){
        CSVLoader loader = new CSVLoader();
        try {
            loader.setSource(context.openFileInput(workerLogin+"data.txt"));

            Instances train=loader.getDataSet();
            train.deleteAttributeAt(0);

            Log.e("train", train.toString());

            train.setClassIndex(train.numAttributes() - 1);
            j48= new J48();


            j48.setOptions(new String[] { "-O"}); // set the options
            j48.buildClassifier(train);


            Log.i("model",j48.toString());
            //saveModel(context,workerLogin);

        }catch (Exception e){
            Log.e("ExTB2", e.toString());
        }


        return j48;
    }

    public J48 readModel (Context context){

        try {
            ObjectInputStream oin = new ObjectInputStream(context.openFileInput(workerLogin+"j48.txt"));
            j48=(J48)oin.readObject();
        }
        catch (Exception e) {
            Log.e("Exception", "File read failed: " + e.toString());
        }

        return j48;
    }

    public J48 saveModel (Context context) {

        try {

            ObjectOutputStream oout = new ObjectOutputStream(context.openFileOutput(workerLogin+"j48.txt", Context.MODE_PRIVATE));

            oout.writeObject(j48);

            oout.close();

        }
        catch (Exception e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
        return j48;
    }
}
