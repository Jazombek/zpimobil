package pl.edu.pwr.zpiclient.Activities;

import android.content.Context;
import android.content.Intent;

import com.google.android.material.navigation.NavigationView;

import androidx.appcompat.app.AppCompatActivity;
import pl.edu.pwr.zpiclient.Adapters.TaskListAdapter;
import pl.edu.pwr.zpiclient.DataClasses.TaskWorker;
import pl.edu.pwr.zpiclient.DataClasses.Worker;
import pl.edu.pwr.zpiclient.R;
import pl.edu.pwr.zpiclient.Services.BackgroundService;

import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.LinkedHashMap;


import static pl.edu.pwr.zpiclient.Activities.MainActivity.apiUri;


public class WorkerTaskListActivity extends AppCompatActivity{
    private Button summaryButton, settingsButton;
    private TextView activeText,plannedText;
    private ListView activeList,plannedList;
    private static ArrayList<TaskWorker> taskC=new ArrayList<>(); //lista zadan pobierana za kazdym odpaleniem worker main
    private static ArrayList<TaskWorker> taskP=new ArrayList<>();
    private TaskListAdapter aktywnyAdapter, planowanyAdapter;
    private static Worker worker;






    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawer_worker_tasks);
        worker=((Worker)getIntent().getSerializableExtra("worker"));
        setContents();
        setListeners();

        /*clearFile(WorkerTaskListActivity.this);


        //requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION},1447);
        final Intent intent = new Intent(this.getApplication(), BackgroundService.class);
        this.getApplication().startService(intent);
//        this.getApplication().startForegroundService(intent);
        this.getApplication().bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE);

        //backService.startTracking();
*/


    }
    @Override
    public void onResume(){
        super.onResume();

        getData();
        setData();

    }

    @Override
    protected void onStart() {
        super.onStart();

    }
    @Override
    protected void onDestroy() {
        /*if(backService!=null)

            backService.stopTracking();*/
        super.onDestroy();
    }


    public static void start(Context context,Worker worker) {
        Intent starter = new Intent(context, WorkerTaskListActivity.class);
        starter.putExtra("worker", worker);
        context.startActivity(starter);
    }
    private void setContents(){
        summaryButton=findViewById(R.id.summaryButton);
        settingsButton=findViewById(R.id.settingsButton);

        activeText=findViewById(R.id.activeTaskText);
        plannedText=findViewById(R.id.plannedTaskText);

        activeList=findViewById(R.id.activeListView);
        plannedList=findViewById(R.id.plannedListView);

        //side=findViewById(R.id.sideT);
        //side.setClickable(true);
    }

    private void setListeners(){
        /*activeList.setClickable(true);
        activeList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Toast.makeText(WorkerTaskListActivity.this, i,Toast.LENGTH_SHORT).show();
                WorkerTaskDetailsActivity.start(WorkerTaskListActivity.this, (TaskWorker) adapterView.getItemAtPosition(i),worker);
            }
        });
        plannedList.setClickable(true);
        plannedList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                WorkerTaskDetailsActivity.start(WorkerTaskListActivity.this, (TaskWorker) adapterView.getItemAtPosition(i),worker);
            }
        });*/







    }




    private void getData(){
        taskC.clear();
        taskP.clear();
        RestTemplate restTemplate = RTemplate.getInstance().restTemplate;
        //try{
            //ArrayList<Task> response = restTemplate.getForEntity("https://10.0.2.2:8434/api/taskList", ArrayList.class).getBody();
            //exchange get with session ID elo benc
            ArrayList<LinkedHashMap> response = restTemplate.exchange(apiUri+"workerTask/"+worker.getId(), HttpMethod.GET, new HttpEntity<String>(RTemplate.getInstance().sessionHeaders), ArrayList.class).getBody();
            //Toast.makeText(WorkerTaskListActivity.this, response.size(),Toast.LENGTH_SHORT).show();
            //tasks=response;

            for (int i = 0; i < response.size(); i++) {
                LinkedHashMap hm = response.get(i);
                int idTask = (int)(hm.get("idTask"));

                HttpHeaders headers=RTemplate.getInstance().sessionHeaders;
                headers.add("idW", String.valueOf(worker.getId()));
                headers.add("idT", String.valueOf(idTask));
                HttpEntity request=new HttpEntity<String>(headers);
                Long response2 = restTemplate.exchange(apiUri + "workerTaskS", HttpMethod.GET, request, Long.class).getBody();

                headers.remove("idW");
                headers.remove("idT");


                TaskWorker t = new TaskWorker((int) (hm.get("idTask")), (String) (hm.get("name")), (String) (hm.get("description")),
                        (String) (hm.get("data")), (int) (hm.get("idSupTask")), (int) (hm.get("idManager")),
                        (double) (hm.get("progress")), (int) (hm.get("idStatus")),Integer.parseInt(String.valueOf(response2)));

                //ArrayList<LinkedHashMap> response2 = restTemplate.exchange(apiUri+"workerTaskS/", HttpMethod.GET, new HttpEntity<String>(RTemplate.getInstance().sessionHeaders), ArrayList.class).getBody();



                if(t.getIdStatus()==1)
                    taskC.add(t);
                else if(t.getIdStatus()==2)
                    taskP.add(t);
                else Log.i("Task id: ",t.getIdStatus()+" "+t.getName());

            }
            //Toast.makeText(getApplicationContext(), response.toString(),Toast.LENGTH_SHORT).show();
        //}catch (Exception e){
        //}
    }

    public void taskDetails(TaskWorker task){
        WorkerTaskDetailsActivity.start(WorkerTaskListActivity.this, task,worker);
    }

    public void taskStatus(TaskWorker task){

        RestTemplate restTemplate = RTemplate.getInstance().restTemplate;
        HttpHeaders headers=RTemplate.getInstance().sessionHeaders;
        headers.add("idW", String.valueOf(worker.getId()));
        headers.add("idS", String.valueOf(task.getIdWorkerStatus()));
        headers.add("idT", String.valueOf(task.getIdTask()));
        HttpEntity request=new HttpEntity<String>(headers);

        restTemplate.exchange(apiUri + "updateWTS", HttpMethod.GET, request, String.class).getBody();
        //Toast.makeText(getApplicationContext(), response,Toast.LENGTH_SHORT).show();
        headers.remove("idW");
        headers.remove("idS");
        headers.remove("idT");
        aktywnyAdapter.notifyDataSetChanged();
        planowanyAdapter.notifyDataSetChanged();
    }
    //String response2 = restTemplate.exchange(apiUri + "updateTaskS2", HttpMethod.GET, request, String.class).getBody();
    private void setData(){


        ArrayList<TaskWorker> aktywne = taskC;
        ArrayList<TaskWorker> planowane = taskP;
        //aktywne.add(new TaskWorker(10,"Stół","drewniany","2019.07.12",12,13,15.7,16,4));
        //aktywne.add(new TaskWorker(10,"Krzesło","drewniane","2019.08.15",12,13,15.7,16,5));
        //planowane.add(new TaskWorker(10,"Biurko","sklejka","2019.08.26",12,13,15.7,16,5));
        /*Task task;
        task=new Task("Pierwszy", "10.02.2019",1);
        aktywne.add(task);
        task=new Task("Drugi", "11.02.2019",2);
        aktywne.add(task);
        task=new Task("Trzeci", "12.02.2019",1);
        aktywne.add(task);
        task=new Task("Pierwszy2", "10.12.2019",2);
        aktywne.add(task);
        task=new Task("Drugi2", "11.12.2019",1);
        aktywne.add(task);
        task=new Task("Trzeci2", "12.12.2019",1);
        aktywne.add(task);*/

        /*task=new Task("Czwarty", "14.02.2019",1);
        planowane.add(task);
        task=new Task("Piaty", "15.02.2019",2);
        planowane.add(task);
        task=new Task("Szosty", "16.02.2019",1);
        planowane.add(task);*/

        aktywnyAdapter = new TaskListAdapter(this, aktywne);


        planowanyAdapter = new TaskListAdapter(this, planowane);

        activeList.setAdapter(aktywnyAdapter);
        plannedList.setAdapter(planowanyAdapter);


    }





}
